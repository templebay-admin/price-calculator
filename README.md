## Pricing Calculator

Pricing calculator is a lightweight Dynamic pricing system. 
Allowing you to dynamically define the prices of your items based on certain factors

## Getting started

Simply clone the repo, run:

```
composer install
```

Create a new MySQL database called 'pricing_colin' 

Now you can simply run the following command THIS COMMAND WILL FAIL IF YOU HAVE NOT CREATED THE 'pricing_colin' DATABASE

```
php artisan pricing-setup 
```

This will take care of generating your .env and testing .env, testing, migrations, and db seeding required for a browser side test.
This will also create a user, credentials:

email/username: admin@email.com
password: password

Once the command has been run you can access the /admin/dashboard route where you will be asked to login, login in with the credentials provided and off you go. 
The admin panel is simple in nature, easy to navigate and should not cause any confusion.

Have Fun, 

Colin
