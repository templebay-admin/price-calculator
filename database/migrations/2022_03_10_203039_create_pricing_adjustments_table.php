<?php

use App\Enums\PriceAdjustmentType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing_adjustments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('venue_id');
            $table->enum('pricing_adjustment_type', PriceAdjustmentType::getValues());
            $table->integer('modify_amount');
            $table->integer('low_age');
            $table->integer('high_age');
            $table->unsignedBigInteger('pricing_option_id');
            $table->unsignedBigInteger('membership_type_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricing_adjustments');
    }
};
