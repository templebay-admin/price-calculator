<?php

use App\Enums\PurchasableType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchasables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->enum('type', PurchasableType::getValues());
            $table->timestamps();
        });

        Schema::create('pricing_option_purchasable', function (Blueprint $table) {
            $table->integer('pricing_option_id');
            $table->integer('purchasable_id');
            $table->integer('basic_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchasables');
        Schema::dropIfExists('pricing_option_purchasable');
    }
};
