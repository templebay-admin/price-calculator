<?php

namespace Database\Seeders;

use App\Models\MembershipType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MembershipTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = ['Bronze Membership', 'Silver Membership', 'Gold Membership', 'Platinum Membership'];
        foreach($names as $name){ MembershipType::create(['name' => $name]); }
    }
}
