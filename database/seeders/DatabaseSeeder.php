<?php

namespace Database\Seeders;

use App\Models\PricingOption;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MembershipTypeSeeder::class);
        $this->call(VenueSeeder::class);
        $this->call(PricingOptionSeeder::class);
        $this->call(PurchasableSeeder::class);
//        $this->call(PricingAdjustmentSeeder::class);
        $this->call(UserSeeder::class);
    }
}
