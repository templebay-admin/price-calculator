<?php

namespace Database\Seeders;

use App\Enums\PurchasableType;
use App\Models\MembershipType;
use App\Models\PricingOption;
use App\Models\Purchasable;
use App\Models\Venue;
use App\Repositories\PurchasableRepository;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class PurchasableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $purchasableRepository = App::make(PurchasableRepository::class);

        $names = [
            [
                'name' => 'Coffee',
                'type' => PurchasableType::Consumable
            ],
            [
                'name' => 'Googles',
                'type' => PurchasableType::Product
            ],
            [
                'name' => 'Yoga Class',
                'type' => PurchasableType::FitnessClass
            ],
            [
                'name' => 'Zumba Class',
                'type' => PurchasableType::FitnessClass
            ],
            [
                'name' => 'Cheese Toastie',
                'type' => PurchasableType::Consumable
            ],
            [
                'name' => 'Sweatband',
                'type' => PurchasableType::Product
            ],
        ];

        foreach($names as $name){

            $request = new Request();

            $request->name = $name['name'];

            $request->purchasable_type_id = $name['type'];

            $pricingOptions = PricingOption::getOptions($name['type']);

            $request->pricingOptions = $pricingOptions;

            $basic_price = [];

            foreach($pricingOptions as $pricingOption){
                $basic_price[$pricingOption->id] = rand(1,400);
            }

            $request->basic_price = $basic_price;

            $purchasableRepository->create($request);
        }

    }
}
