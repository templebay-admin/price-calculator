<?php

namespace Database\Seeders;

use App\Enums\PriceAdjustmentType;
use App\Models\PricingAdjustment;
use App\Models\Venue;
use Illuminate\Database\Seeder;

class PricingAdjustmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $venues = Venue::all();


        foreach($venues as $venue){
            foreach($venue->purchasables as $purchasable){
                PricingAdjustment::create([
                    'venue_id' => $venue->id,
                    'pricing_adjustment_type' => PriceAdjustmentType::Multiply,
                    'modify_amount' => 1.3,
                    'low_age' => 14,
                    'high_age' => 18,
                    'pricing_option_id' => $purchasable->pivot->pricing_option_id,
                ]);
            }
        }

    }
}
