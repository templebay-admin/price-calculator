<?php

namespace Database\Seeders;

use App\Models\MembershipType;
use App\Models\User;
use App\Models\Venue;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'  => 'Test User',
            'email' => 'admin@email.com',
            'date_of_birth' => '1989-08-01',
            'membership_type_id' => MembershipType::first()->id,
            'password' => 'password',
            'is_admin' => true,
        ]);
    }
}
