<?php

namespace Database\Seeders;

use App\Models\MembershipType;
use App\Models\Venue;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class VenueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Cambridge',
            'Lincoln',
            'Oxford',
            'Bath',
            'Newcastle',
            'Manchester',
            'Sheffield',
            'Leicester',
            'London',
        ];
        foreach($names as $name){ Venue::create(['name' => $name]); }
    }
}
