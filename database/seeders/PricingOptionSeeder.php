<?php

namespace Database\Seeders;

use App\Models\MembershipType;
use App\Models\PricingOption;
use App\Models\Venue;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PricingOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Small',
            'Medium',
            'Large',
            'Standard',
        ];
        foreach($names as $name){ PricingOption::create(['name' => $name]); }
    }
}
