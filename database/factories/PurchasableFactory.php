<?php

namespace Database\Factories;

use App\Enums\PurchasableType;
use Illuminate\Database\Eloquent\Factories\Factory;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @extends Factory
 */
class PurchasableFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    #[ArrayShape([
        'name'               => "string",
        'type'               => "int",
    ])]
    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'type' => PurchasableType::Product
        ];
    }

}
