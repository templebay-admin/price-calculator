<?php

namespace Database\Factories;

use App\Models\MembershipType;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @extends Factory
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    #[ArrayShape([
        'name'               => "string",
        'email'              => "string",
        'password'           => "string",
        'remember_token'     => "string",
        'date_of_birth'      => "string",
        'membership_type_id' => "int"
    ])]
    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => 'password',
            'remember_token' => Str::random(10),
            'date_of_birth' => '1989-01-08',
            'membership_type_id' => 1,
        ];
    }

    /**
     * Indicate that the user is Super Admin.
     *
     * @return Factory
     */
    public function isAdmin(): Factory
    {
        return $this->state(function (array $attributes) {
            return [
                'is_admin' => 1,
            ];
        });
    }

}
