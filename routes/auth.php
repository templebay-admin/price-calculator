<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\RegisteredUserController;

$limiter = config('authentication.limiters.login');
$twoFactorLimiter = config('authentication.limiters.two-factor');
$verificationLimiter = config('authentication.limiters.verification', '6,1');

Route::get('login', [AuthenticatedSessionController::class, 'create'])
    ->middleware(['guest:' . config('authentication.guard')])
    ->name('login');

Route::post('login', [AuthenticatedSessionController::class, 'store'])
    ->middleware(array_filter([
        'guest:'.config('authentication.guard'),
        $limiter ? 'throttle:'.$limiter : null,
    ]))
    ->name('login');

Route::post('logout', [AuthenticatedSessionController::class, 'destroy'])
    ->name('logout');

Route::get('register', [RegisteredUserController::class, 'create'])
    ->middleware(['guest:' . config('authentication.guard')])
    ->name('register');

Route::post('register', [RegisteredUserController::class, 'store'])
    ->middleware(['guest:' . config('authentication.guard')])
    ->name('register');
