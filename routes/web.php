<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\MembershipTypeController;
use App\Http\Controllers\PricingAdjustmentController;
use App\Http\Controllers\PricingOptionController;
use App\Http\Controllers\PurchasableController;
use App\Http\Controllers\VenueController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () { return view('welcome');})->name('home');

Route::group(['middleware' => config('authentication.middleware', ['web'])], __DIR__ . '/auth.php');

Route::middleware(['middleware' => 'auth', 'isAdmin'])->prefix('admin')->name('admin.')->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::get('venues', [VenueController::class, 'index'])->name('venues.index');
    Route::post('venues', [VenueController::class, 'store'])->name('venues.store');
    Route::patch('venues/{venue}', [VenueController::class, 'update'])->name('venues.update');
    Route::delete('venues/{venue}', [VenueController::class, 'destroy'])->name('venues.destroy');

    Route::get('membership-types', [MembershipTypeController::class, 'index'])->name('membership-types.index');
    Route::post('membership-types', [MembershipTypeController::class, 'store'])->name('membership-types.store');
    Route::patch('membership-types/{membershipType}', [MembershipTypeController::class, 'update'])->name('membership-types.update');
    Route::delete('membership-types/{membershipType}', [MembershipTypeController::class, 'destroy'])->name('membership-types.destroy');

    Route::post('pricing-options', [PricingOptionController::class, 'store'])->name('pricing-options.store');
    Route::patch('pricing-options/{pricingOption}', [PricingOptionController::class, 'update'])->name('pricing-options.update');
    Route::delete('pricing-options/{pricingOption}', [PricingOptionController::class, 'destroy'])->name('pricing-options.destroy');

    Route::get('purchasables', [PurchasableController::class, 'index'])->name('purchasables.index');
    Route::post('purchasables', [PurchasableController::class, 'store'])->name('purchasables.store');
    Route::patch('purchasables/{purchasable}', [PurchasableController::class, 'update'])->name('purchasables.update');
    Route::delete('purchasables/{purchasable}', [PurchasableController::class, 'destroy'])->name('purchasables.destroy');

    Route::get('pricing-adjustments', [PricingAdjustmentController::class, 'index'])->name('pricing-adjustments.index');
    Route::post('pricing-adjustments', [PricingAdjustmentController::class, 'store'])->name('pricing-adjustments.store');
    Route::patch('pricing-adjustments/{pricingAdjustment}', [PricingAdjustmentController::class, 'update'])->name('pricing-adjustments.update');
    Route::delete('pricing-adjustments/{pricingAdjustment}', [PricingAdjustmentController::class, 'destroy'])->name('pricing-adjustments.destroy');

});
