<?php

namespace Tests\Traits;

use Illuminate\Testing\TestResponse;
use JetBrains\PhpStorm\ArrayShape;

trait PricingOptionTraits
{
    protected function postRequest($name, $price): TestResponse
    {
        return $this->post('admin/pricing-options', [
            'name' => $name,
        ]);
    }

    protected function patchRequest($name, $pricingOption): TestResponse
    {
        return $this->patch('admin/pricing-options/' . $pricingOption->id, [
            'name' => $name,
        ]);
    }

}
