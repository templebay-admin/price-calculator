<?php

namespace Tests\Traits;

use App\Models\MembershipType;
use Illuminate\Testing\TestResponse;
use JetBrains\PhpStorm\ArrayShape;

trait VenueTraits
{
    protected function postRequest($name): TestResponse
    {
        return $this->post('admin/venues', [
            'name' => $name
        ]);
    }

    protected function patchRequest($name, $venue): TestResponse
    {
        return $this->patch('admin/venues/' . $venue->id, [
            'name' => $name
        ]);
    }

}
