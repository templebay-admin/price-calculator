<?php

namespace Tests\Traits;

use App\Models\MembershipType;
use Illuminate\Testing\TestResponse;
use JetBrains\PhpStorm\ArrayShape;

trait UserTestTraits
{
    protected function registrationPostRequest($field = null, $type = null): TestResponse
    {
        $fieldArray = $this->modifyFieldArray($field, $type);

        return $this->post('/register', $fieldArray);
    }

    #[ArrayShape([
        'name' => "string",
        'email' => "string",
        'date_of_birth' => "string",
        'membership_type_id' => "int",
        'password' => "string",
        'password_confirmation' => "string"
    ])] private function fieldArray(): array
    {
        return [
            'name'  => 'Test Name',
            'email' => 'test@email.com',
            'date_of_birth' => '1989-08-01',
            'membership_type_id' => MembershipType::factory()->create()->id,
            'password' => 'password',
            'password_confirmation' => 'password',
        ];
    }

    protected function loginPostRequest($email, $password): TestResponse
    {
        return $this->post('/login', [
            'email'    => $email,
            'password' => $password
        ]);
    }

    protected function logoutPostRequest(): TestResponse
    {
        return $this->post('/logout');
    }

    private function modifyFieldArray($field, $type): array
    {
        $fieldArray = $this->fieldArray();

        if($field !== null && $type !== null){
            return $this->$type($field, $fieldArray);
        }

        return $fieldArray;
    }

    private function remove($field, $fieldArray)
    {
        $fieldArray[$field] = '';
        return $fieldArray;
    }

    private function mutate($field, $fieldArray)
    {
        $fieldArray[$field] = strtoupper($fieldArray[$field]);
        return $fieldArray;
    }

    private function exceed($field, $fieldArray)
    {
        $fieldArray[$field] = $this->randomString(256);
        return $fieldArray;
    }

    private function stringify($field, $fieldArray)
    {
        $fieldArray[$field] = $this->randomString(13);
        return $fieldArray;
    }

    private function replaceId($field, $fieldArray)
    {
        $fieldArray[$field] = 7;
        return $fieldArray;
    }
}
