<?php

namespace Tests\Traits;

use App\Models\PricingOption;
use App\Models\Purchasable;
use Illuminate\Testing\TestResponse;

trait PurchasableTraits
{
    protected function postRequest($name, $purchasableTypeId): TestResponse
    {
        $pricingOptions = PricingOption::getOptions($purchasableTypeId);

        $basic_price = [];

        foreach($pricingOptions as $pricingOption){
            $basic_price[$pricingOption->id] = rand(1,400);
        }

        return $this->post('admin/purchasables', [
            'name'                => $name,
            'purchasable_type_id' => $purchasableTypeId,
            'pricingOptions'      => $pricingOptions,
            'basic_price'         => $basic_price,
        ]);
    }

    protected function patchRequest($name, $purchasable): TestResponse
    {
        return $this->patch('admin/purchasables/'. $purchasable->id, [
            'name'                => $name,
            'purchasable_type_id' => $purchasable->type,
        ]);
    }

    protected function createPurchasable()
    {
        $purchasable = Purchasable::factory()->create();

        $pricingOptions = PricingOption::getOptions($purchasable->type);

        foreach($pricingOptions as $pricingOption){
            $purchasable->pricingOptions()->attach($pricingOption,[
                'basic_price' => rand(1,400)
            ]);
        }

        return $purchasable;
    }

}
