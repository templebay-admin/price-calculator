<?php

namespace Tests\Traits;

use App\Enums\PriceAdjustmentType;
use App\Models\MembershipType;
use App\Models\PricingAdjustment;
use App\Models\PricingOption;
use App\Models\Purchasable;
use App\Models\Venue;
use Illuminate\Testing\TestResponse;

trait PricingAdjustmentTraits
{
    protected function postRequest(): TestResponse
    {
        $venue = Venue::factory()->create();
        $membershipType = MembershipType::factory()->create();
        $pricingOption = PricingOption::factory()->create();

        return $this->post('admin/pricing-adjustments', [
            'venue_id' => $venue->id,
            'pricing_adjustment_type' => PriceAdjustmentType::Multiply,
            'low_age' => 20,
            'high_age' => 40,
            'modify_amount' => 0.8,
            'pricing_option_id' => $pricingOption->id,
            'membership_type_id' => $membershipType->id
        ]);
    }

    protected function patchRequest($name, $purchasable): TestResponse
    {
        return $this->patch('admin/purchasables/'. $purchasable->id, [
            'name'                => $name,
            'purchasable_type_id' => $purchasable->type,
        ]);
    }

    protected function createPurchasable()
    {
        $purchasable = Purchasable::factory()->create();

        $pricingOptions = PricingOption::getOptions($purchasable->type);

        foreach($pricingOptions as $pricingOption){
            $purchasable->pricingOptions()->attach($pricingOption,[
                'basic_price' => rand(1,400)
            ]);
        }

        return $purchasable;
    }

}
