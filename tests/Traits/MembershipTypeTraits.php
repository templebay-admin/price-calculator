<?php

namespace Tests\Traits;

use App\Models\MembershipType;
use Illuminate\Testing\TestResponse;
use JetBrains\PhpStorm\ArrayShape;

trait MembershipTypeTraits
{
    protected function postRequest($name): TestResponse
    {
        return $this->post('admin/membership-types', [
            'name' => $name
        ]);
    }

    protected function patchRequest($name, $membershipType): TestResponse
    {
        return $this->patch('admin/membership-types/' . $membershipType->id, [
            'name' => $name
        ]);
    }

}
