<?php

namespace Tests\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Testing\TestResponse;

trait TestTraits
{
    /**
     * Generate a random string
     *
     * @param $length
     * @return string
     */
    protected function randomString($length): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * return a local png image for testing image uploads
     *
     */
    protected function testPngImage(): UploadedFile
    {
        return new UploadedFile(resource_path('test-files/test.png'), 'test.png', 'image/png', null, true);
    }

    /**
     * return a local jpg image for testing image uploads
     *
     */
    protected function testJpgImage(): UploadedFile
    {
        return new UploadedFile(resource_path('test-files/test.jpeg'), 'test.jpeg', 'image/jpg', null, true);
    }

    /**
     * return a local jpeg image for testing image uploads
     *
     */
    protected function testJpegImage(): UploadedFile
    {
        return new UploadedFile(resource_path('test-files/test.jpeg'), 'test.jpeg', 'image/jpeg', null, true);
    }

    /**
     * return a local jpeg image for testing image uploads
     *
     */
    protected function testPdfFile(): UploadedFile
    {
        return new UploadedFile(resource_path('test-files/test.pdf'), 'test.pdf', 'application/pdf', null, true);
    }

}
