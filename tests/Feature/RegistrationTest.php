<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Tests\Traits\TestTraits;
use Tests\Traits\UserTestTraits;

class RegistrationTest extends TestCase
{
    use RefreshDatabase, UserTestTraits, TestTraits;

    /**
     * @test
     *
     * Test to see if a non-authenticated user can access the register uri
     */
    public function a_non_authenticated_user_can_access_the_register_uri()
    {
        $response = $this->get('/register');

        $response->assertOk();
    }

    /**
     * @test
     *
     * Test to see if an authenticated user can't access the register uri
     */
    public function an_authenticated_user_cant_access_the_register_uri()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $response = $this->get('/register');

        $response->assertRedirect();
    }

    /**
     * @test
     *
     * Test to see if a non-authenticated user can register
     */
    public function a_non_authenticated_user_can_register()
    {
        $response = $this->registrationPostRequest();

        $response->assertRedirect(route('admin.dashboard'));

        $this->assertCount(1, User::all());
    }

    /**
     * @test
     *
     * Test to see if an authenticated user can't register
     */
    public function an_authenticated_user_cant_register()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $response = $this->registrationPostRequest();

        $response->assertRedirect(route('home'));

        $this->assertCount(1, User::all());
    }

    /**
     * @test
     *
     * Test that name is required
     */
    public function name_is_required()
    {
        $response = $this->registrationPostRequest('name', 'remove');

        $response->assertSessionHasErrors('name');

        $this->assertCount(0, User::all());
    }

    /**
     * @test
     *
     * Test that name can not exceed 255
     */
    public function name_cannot_exceed_255_characters()
    {
        $response = $this->registrationPostRequest('name', 'exceed');

        $response->assertSessionHasErrors('name');

        $this->assertCount(0, User::all());
    }

    /**
     * @test
     *
     * Test that email is required
     */
    public function email_is_required()
    {
        $response = $this->registrationPostRequest('email', 'remove');

        $response->assertSessionHasErrors('email');

        $this->assertCount(0, User::all());
    }

    /**
     * @test
     *
     * Test that email can not exceed 255
     */
    public function email_cannot_exceed_255_characters()
    {
        $response = $this->registrationPostRequest('email', 'exceed');

        $response->assertSessionHasErrors('email');

        $this->assertCount(0, User::all());
    }

    /**
     * @test
     *
     * Test that email must be email address
     */
    public function email_must_be_email()
    {
        $response = $this->registrationPostRequest('email', 'stringify');

        $response->assertSessionHasErrors('email');

        $this->assertCount(0, User::all());
    }

    /**
     * @test
     *
     * Test that email must be unique
     */
    public function email_must_be_unique()
    {
        $response = $this->registrationPostRequest();

        $response->assertRedirect(route('admin.dashboard'));

        $this->assertCount(1, User::all());

        $this->logoutPostRequest();

        $responseTwo = $this->registrationPostRequest();

        $responseTwo->assertSessionHasErrors('email');

        $this->assertCount(1, User::all());
    }

    /**
     * @test
     *
     * Test that membership_type_id is required
     */
    public function membership_type_id_is_required()
    {
        $response = $this->registrationPostRequest('membership_type_id', 'remove');

        $response->assertSessionHasErrors('membership_type_id');

        $this->assertCount(0, User::all());
    }

    /**
     * @test
     *
     * Test that membership_type_id must exist
     */
    public function membership_type_id_must_exist()
    {
        $response = $this->registrationPostRequest('membership_type_id', 'replaceId');

        $response->assertSessionHasErrors('membership_type_id');

        $this->assertCount(0, User::all());
    }


    /**
     * @test
     *
     * Test that date_of_birth is required
     */
    public function date_of_birth_is_required()
    {
        $response = $this->registrationPostRequest('date_of_birth', 'remove');

        $response->assertSessionHasErrors('date_of_birth');

        $this->assertCount(0, User::all());
    }

    /**
     * @test
     *
     * Test that date_of_birth is date
     */
    public function date_of_birth_is_date()
    {
        $response = $this->registrationPostRequest('date_of_birth', 'stringify');

        $response->assertSessionHasErrors('date_of_birth');

        $this->assertCount(0, User::all());
    }

    /**
     * @test
     *
     * Test that password is required
     */
    public function password_is_required()
    {
        $response = $this->registrationPostRequest('password', 'remove');

        $response->assertSessionHasErrors('password');

        $this->assertCount(0, User::all());
    }

    /**
     * @test
     *
     * Test that password is confirmed
     */
    public function password_is_confirmed()
    {
        $response = $this->registrationPostRequest('password_confirmation', 'remove');

        $response->assertSessionHasErrors('password');

        $this->assertCount(0, User::all());
    }
}
