<?php

namespace Tests\Feature;

use App\Models\PricingOption;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\PricingOptionTraits;
use Tests\Traits\TestTraits;

class PricingOptionUpdateTest extends TestCase
{
    use RefreshDatabase, PricingOptionTraits, TestTraits;


    /**
     * @test
     *
     * Test to see if a non-authenticated user can't update a venue
     */
    public function a_non_authenticated_user_cant_update_a_venue()
    {
        $pricingOption = PricingOption::factory()->create();

        $response = $this->patchRequest('Test pricing option', $pricingOption);

        $response->assertRedirect(route('login'));

        $this->assertNotEquals('Test pricing option', $pricingOption->name);
    }

    /**
     * @test
     *
     * Test to see if a non-admin user can't update a venue
     */
    public function a_non_admin_user_cant_update_a_venue()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $pricingOption = PricingOption::factory()->create();

        $response = $this->patchRequest('Test pricing option', $pricingOption);

        $response->assertRedirect(route('admin.dashboard'));

        $this->assertNotEquals('Test pricing option', $pricingOption->name);
    }

    /**
     * @test
     *
     * Test to see if an admin user can update a venue
     */
    public function an_admin_user_can_update_a_venue()
    {
        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $pricingOption = PricingOption::factory()->create();

        $response = $this->patchRequest('Test pricing option', $pricingOption);

        $response->assertOk();

        $this->assertEquals('Test pricing option', $pricingOption->fresh()->name);
    }



}
