<?php

namespace Tests\Feature;

use App\Models\MembershipType;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\MembershipTypeTraits;
use Tests\Traits\TestTraits;

class MembershipTypeUpdateTest extends TestCase
{
    use RefreshDatabase, MembershipTypeTraits, TestTraits;

    /**
     * @test
     *
     * Test to see if a non-authenticated user can't update a membership plan
     */
    public function a_non_authenticated_user_cant_update_a_membership_plan()
    {
        $membershipType = MembershipType::factory()->create();

        $response = $this->patchRequest('Test Membership Plan', $membershipType);

        $response->assertRedirect(route('login'));

        $this->assertNotEquals('Test Membership Plan', $membershipType->name);
    }

    /**
     * @test
     *
     * Test to see if a non-admin user can't update a membership plan
     */
    public function a_non_admin_user_cant_update_a_membership_plan()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $membershipType = MembershipType::factory()->create();

        $response = $this->patchRequest('Test Membership Plan', $membershipType);

        $response->assertRedirect(route('admin.dashboard'));

        $this->assertNotEquals('Test Membership Plan', $membershipType->name);
    }

    /**
     * @test
     *
     * Test to see if an admin user can update a membership plan
     */
    public function an_admin_user_can_update_a_membership_plan()
    {
        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $membershipType = MembershipType::factory()->create();

        $response = $this->patchRequest('Test Membership Plan', $membershipType);

        $response->assertOk();

        $this->assertEquals('Test Membership Plan', $membershipType->fresh()->name);
    }



}
