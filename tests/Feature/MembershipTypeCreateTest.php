<?php

namespace Tests\Feature;

use App\Models\MembershipType;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\MembershipTypeTraits;
use Tests\Traits\TestTraits;

class MembershipTypeCreateTest extends TestCase
{
    use RefreshDatabase, MembershipTypeTraits, TestTraits;

    /**
     * @test
     *
     * Test to see if a non-authenticated user can't create a membership plan
     */
    public function a_non_authenticated_user_cant_create_a_membership_plan()
    {
        $response = $this->postRequest('Test Membership Plan');

        $response->assertRedirect(route('login'));

        $this->assertCount(0, MembershipType::all());
    }

    /**
     * @test
     *
     * Test to see if a non-admin user can't create a membership plan
     */
    public function a_non_admin_user_cant_create_a_membership_plan()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $response = $this->postRequest('Test Membership Plan');

        $response->assertRedirect(route('admin.dashboard'));

        $this->assertCount(0, MembershipType::all());
    }

    /**
     * @test
     *
     * Test to see if an admin user can create a membership plan
     */
    public function an_admin_user_can_create_a_membership_plan()
    {
        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $response = $this->postRequest('Test Membership Plan');

        $response->assertOk();

        $this->assertCount(1, MembershipType::all());
    }

    /**
     * @test
     *
     * Name field is required
     */
    public function name_field_is_required()
    {
        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $response = $this->postRequest('');

        $response->assertSessionHasErrors('name');

        $this->assertCount(0, MembershipType::all());
    }

    /**
     * @test
     *
     * Name field can't exceed 255 characters
     */
    public function name_field_cant_exceed_255_characters()
    {
        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $response = $this->postRequest($this->randomString(256));

        $response->assertSessionHasErrors('name');

        $this->assertCount(0, MembershipType::all());
    }



}
