<?php

namespace Tests\Feature;

use App\Enums\PurchasableType;
use App\Models\User;
use App\Models\Purchasable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\PurchasableTraits;
use Tests\Traits\TestTraits;

class PurchasableCreateTest extends TestCase
{
    use RefreshDatabase, PurchasableTraits, TestTraits;

    /**
     * @test
     *
     * Test to see if a non-authenticated user can't create a purchasable
     */
    public function a_non_authenticated_user_cant_create_a_purchasable()
    {

        $response = $this->postRequest('Test Purchasable', PurchasableType::FitnessClass);

        $response->assertRedirect(route('login'));

        $this->assertCount(0, Purchasable::all());
    }

    /**
     * @test
     *
     * Test to see if a non-admin user can't create a purchasable
     */
    public function a_non_admin_user_cant_create_a_purchasable()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $response = $this->postRequest('Test Purchasable', PurchasableType::FitnessClass);

        $response->assertRedirect(route('admin.dashboard'));

        $this->assertCount(0, Purchasable::all());
    }

    /**
     * @test
     *
     * Test to see if an admin user can create a purchasable
     */
    public function an_admin_user_can_create_a_purchasable()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $response = $this->postRequest('Test Purchasable', PurchasableType::FitnessClass);

        $response->assertOk();

        $this->assertCount(1, Purchasable::all());
    }

    /**
     * @test
     *
     * Name field is required
     */
    public function name_field_is_required()
    {
        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $response = $this->postRequest('', PurchasableType::FitnessClass);

        $response->assertSessionHasErrors('name');

        $this->assertCount(0, Purchasable::all());
    }

    /**
     * @test
     *
     * Name field can't exceed 255 characters
     */
    public function name_field_cant_exceed_255_characters()
    {
        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $response = $this->postRequest($this->randomString(256), PurchasableType::FitnessClass);

        $response->assertSessionHasErrors('name');

        $this->assertCount(0, Purchasable::all());
    }



}
