<?php

namespace Tests\Feature;

use App\Models\PricingAdjustment;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\PricingAdjustmentTraits;
use Tests\Traits\TestTraits;

class PricingAdjustmentCreateTest extends TestCase
{
    use RefreshDatabase, PricingAdjustmentTraits, TestTraits;

    /**
     * @test
     *
     * Test to see if a non-authenticated user can't create a purchasable
     */
    public function a_non_authenticated_user_cant_create_a_purchasable()
    {
        $response = $this->postRequest();

        $response->assertRedirect(route('login'));

        $this->assertCount(0, PricingAdjustment::all());
    }

    /**
     * @test
     *
     * Test to see if a non-admin user can't create a purchasable
     */
    public function a_non_admin_user_cant_create_a_purchasable()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $response = $this->postRequest();

        $response->assertRedirect(route('admin.dashboard'));

        $this->assertCount(0, PricingAdjustment::all());
    }

    /**
     * @test
     *
     * Test to see if an admin user can create a purchasable
     */
    public function an_admin_user_can_create_a_purchasable()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $response = $this->postRequest();

        $response->assertOk();

        $this->assertCount(1, PricingAdjustment::all());
    }



}
