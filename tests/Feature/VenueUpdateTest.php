<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Venue;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\TestTraits;
use Tests\Traits\VenueTraits;

class VenueUpdateTest extends TestCase
{
    use RefreshDatabase, VenueTraits, TestTraits;


    /**
     * @test
     *
     * Test to see if a non-authenticated user can't update a venue
     */
    public function a_non_authenticated_user_cant_update_a_venue()
    {
        $venue = Venue::factory()->create();

        $response = $this->patchRequest('Test venue', $venue);

        $response->assertRedirect(route('login'));

        $this->assertNotEquals('Test venue', $venue->name);
    }

    /**
     * @test
     *
     * Test to see if a non-admin user can't update a venue
     */
    public function a_non_admin_user_cant_update_a_venue()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $venue = Venue::factory()->create();

        $response = $this->patchRequest('Test venue', $venue);

        $response->assertRedirect(route('admin.dashboard'));

        $this->assertNotEquals('Test venue', $venue->name);
    }

    /**
     * @test
     *
     * Test to see if an admin user can update a venue
     */
    public function an_admin_user_can_update_a_venue()
    {
        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $venue = Venue::factory()->create();

        $response = $this->patchRequest('Test venue', $venue);

        $response->assertOk();

        $this->assertEquals('Test venue', $venue->fresh()->name);
    }



}
