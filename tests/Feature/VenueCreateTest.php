<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Venue;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\TestTraits;
use Tests\Traits\VenueTraits;

class VenueCreateTest extends TestCase
{
    use RefreshDatabase, VenueTraits, TestTraits;

    /**
     * @test
     *
     * Test to see if a non-authenticated user can't create a membership plan
     */
    public function a_non_authenticated_user_cant_create_a_venue()
    {

        $response = $this->postRequest('Test Venue');

        $response->assertRedirect(route('login'));

        $this->assertCount(0, Venue::all());
    }

    /**
     * @test
     *
     * Test to see if a non-admin user can't create a Venue
     */
    public function a_non_admin_user_cant_create_a_venue()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $response = $this->postRequest('Test Venue');

        $response->assertRedirect(route('admin.dashboard'));

        $this->assertCount(0, Venue::all());
    }

    /**
     * @test
     *
     * Test to see if an admin user can create a Venue
     */
    public function an_admin_user_can_create_a_venue()
    {
        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $response = $this->postRequest('Test Venue');

        $response->assertOk();

        $this->assertCount(1, Venue::all());
    }

    /**
     * @test
     *
     * Name field is required
     */
    public function name_field_is_required()
    {
        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $response = $this->postRequest('');

        $response->assertSessionHasErrors('name');

        $this->assertCount(0, Venue::all());
    }

    /**
     * @test
     *
     * Name field can't exceed 255 characters
     */
    public function name_field_cant_exceed_255_characters()
    {
        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $response = $this->postRequest($this->randomString(256));

        $response->assertSessionHasErrors('name');

        $this->assertCount(0, Venue::all());
    }



}
