<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\TestTraits;
use Tests\Traits\UserTestTraits;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase, UserTestTraits, TestTraits;

    /**
     * @test
     *
     * Test to see if a non-authenticated user can access the login uri
     */
    public function a_non_authenticated_user_can_access_the_login_uri()
    {
        $response = $this->get('/login');

        $response->assertOk();
    }

    /**
     * @test
     *
     * Test to see if an authenticated user can't access the login uri
     */
    public function an_authenticated_user_cant_access_the_login_uri()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $response = $this->get('/login');

        $response->assertRedirect();
    }

    /**
     * @test
     *
     * Test to see if a non-authenticated user can login
     */
    public function a_non_authenticated_user_can_login()
    {
        $user = User::factory()->create();

        $response = $this->loginPostRequest($user->email, 'password');

        $response->assertRedirect(route('admin.dashboard'));

        $this->assertCount(1, User::all());
    }

    /**
     * @test
     *
     * Test to see if an authenticated user can't login
     */
    public function an_authenticated_user_cant_register()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $response = $this->loginPostRequest($user->email, 'password');

        $response->assertRedirect(route('home'));

        $this->assertCount(1, User::all());
    }

    /**
     * @test
     *
     * Test to ensure email is required
     */
    public function an_email_address_is_required()
    {
        $user = User::factory()->create();

        $response = $this->loginPostRequest('', 'password');

        $response->assertSessionHasErrors('email');

        $this->assertCount(1, User::all());
    }

    /**
     * @test
     *
     * Test to ensure password is required
     */
    public function a_password_address_is_required()
    {
        $user = User::factory()->create();

        $response = $this->loginPostRequest($user->email, '');

        $response->assertSessionHasErrors('password');

        $this->assertCount(1, User::all());
    }


    /**
     * @test
     *
     * Test to ensure password is correct
     */
    public function password_must_be_correct()
    {
        $user = User::factory()->create();

        $response = $this->loginPostRequest($user->email, 'passwor');

        $response->assertSessionHasErrors('email');

        $this->assertCount(1, User::all());
    }
}
