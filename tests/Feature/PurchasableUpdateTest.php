<?php

namespace Tests\Feature;

use App\Models\PricingOption;
use App\Models\User;
use App\Models\Purchasable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\TestTraits;
use Tests\Traits\PurchasableTraits;

class PurchasableUpdateTest extends TestCase
{
    use RefreshDatabase, PurchasableTraits, TestTraits;


    /**
     * @test
     *
     * Test to see if a non-authenticated user can't update a purchasable
     */
    public function a_non_authenticated_user_cant_update_a_purchasable()
    {
        $purchasable = $this->createPurchasable();

        $response = $this->patchRequest('Test purchasable', $purchasable);

        $response->assertRedirect(route('login'));

        $this->assertNotEquals('Test purchasable', $purchasable->name);
    }

    /**
     * @test
     *
     * Test to see if a non-admin user can't update a purchasable
     */
    public function a_non_admin_user_cant_update_a_purchasable()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $purchasable = $this->createPurchasable();

        $response = $this->patchRequest('Test purchasable', $purchasable);

        $response->assertRedirect(route('admin.dashboard'));

        $this->assertNotEquals('Test purchasable', $purchasable->name);
    }

    /**
     * @test
     *
     * Test to see if an admin user can update a purchasable
     */
    public function an_admin_user_can_update_a_purchasable()
    {
        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $purchasable = $this->createPurchasable();

        $response = $this->patchRequest('Test purchasable', $purchasable);

        $response->assertOk();

        $this->assertEquals('Test purchasable', $purchasable->fresh()->name);
    }



}
