<?php

namespace Tests\Feature;

use App\Models\MembershipType;
use App\Models\PricingOption;
use App\Models\User;
use App\Models\Venue;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Tests\Traits\MembershipTypeTraits;
use Tests\Traits\PricingOptionTraits;
use Tests\Traits\TestTraits;
use Tests\Traits\UserTestTraits;
use Tests\Traits\VenueTraits;

class PricingOptionCreateTest extends TestCase
{
    use RefreshDatabase, PricingOptionTraits, TestTraits;

    /**
     * @test
     *
     * Test to see if a non-authenticated user can't create a pricing option
     */
    public function a_non_authenticated_user_cant_create_a_pricing_option()
    {
        $response = $this->postRequest('Test Pricing Option', 50);

        $response->assertRedirect(route('login'));

        $this->assertCount(0, PricingOption::all());
    }

    /**
     * @test
     *
     * Test to see if a non-admin user can't create a Venue
     */
    public function a_non_admin_user_cant_create_a_pricing_option()
    {
        $user = User::factory()->create();

        $this->actingAs($user);

        $response = $this->postRequest('Test Pricing Option', 50);

        $response->assertRedirect(route('admin.dashboard'));

        $this->assertCount(0, PricingOption::all());
    }

    /**
     * @test
     *
     * Test to see if an admin user can create a Venue
     */
    public function an_admin_user_can_create_a_pricing_option()
    {
        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $response = $this->postRequest('Test Pricing Option', 50);

        $response->assertOk();

        $this->assertCount(1, PricingOption::all());
    }

    /**
     * @test
     *
     * Name field is required
     */
    public function name_field_is_required()
    {
        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $response = $this->postRequest('', 50);

        $response->assertSessionHasErrors('name');

        $this->assertCount(0, PricingOption::all());
    }

    /**
     * @test
     *
     * Name field can't exceed 255 characters
     */
    public function name_field_cant_exceed_255_characters()
    {
        $user = User::factory()->isAdmin()->create();

        $this->actingAs($user);

        $response = $this->postRequest($this->randomString(256), 50);

        $response->assertSessionHasErrors('name');

        $this->assertCount(0, PricingOption::all());
    }


}
