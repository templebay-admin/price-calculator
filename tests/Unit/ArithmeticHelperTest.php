<?php

namespace Tests\Unit;

use App\Helpers\Math\ArithmeticHelper;
use PHPUnit\Framework\TestCase;

class ArithmeticHelperTest extends TestCase
{
    public function test_add_can_sum_numbers()
    {
        $num1 = 5;
        $num2 = 10;
        $sum = $num1 + $num2;

        $result = ArithmeticHelper::add($num1, $num2);

        $this->assertSame($sum, $result, "add function not added correctly");
    }

    public function test_add_can_take_in_multiple_arguments()
    {
        $num1 = 5;
        $num2 = 10;
        $num3 = 40;
        $sum = $num1 + $num2 + $num3;

        $result = ArithmeticHelper::add($num1, $num2, $num3);

        $this->assertSame($sum, $result, "add function not taking multiple arguments");
    }

    public function test_add_cannot_take_in_array_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::add(["abc"]);
    }

    public function test_add_cannot_take_in_string_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::add("abc");
    }

    public function test_add_cannot_take_in_null_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::add(null);
    }

    public function test_add_cannot_take_in_boolean_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::add(true);
    }

    public function test_add_cannot_take_in_function_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::add(fn() => true);
    }

    public function test_add_needs_at_least_one_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::add();
    }

    public function test_minus_can_subtract_numbers()
    {
        $num1 = 10;
        $num2 = 5;
        $sum = $num1 - $num2;

        $result = ArithmeticHelper::minus($num1, $num2);

        $this->assertSame($sum, $result, "minus function not subtracting correctly");
    }

    public function test_minus_argument_one_cannot_take_in_array_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::minus(["abc"], 10);
    }

    public function test_minus_argument_one_cannot_take_in_string_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::minus("abc", 10);
    }

    public function test_minus_argument_one_cannot_take_in_null_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::minus(null, 10);
    }

    public function test_minus_argument_one_cannot_take_in_boolean_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::minus(true, 10);
    }

    public function test_minus_argument_one_cannot_take_in_function_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::minus(fn() => true, 10);
    }

    public function test_minus_argument_two_cannot_take_in_array_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::minus(10, ["abc"]);
    }

    public function test_minus_argument_two_cannot_take_in_string_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::minus(10, "abc");
    }

    public function test_minus_argument_two_cannot_take_in_null_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::minus(10, null);
    }

    public function test_minus_argument_two_cannot_take_in_boolean_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::minus(10, true);
    }

    public function test_minus_argument_two_cannot_take_in_function_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::minus(10, fn() => true);
    }

    public function test_multiply_can_multiply_numbers()
    {
        $num1 = 10;
        $num2 = 5;
        $sum = $num1 * $num2;

        $result = ArithmeticHelper::multiply($num1, $num2);

        $this->assertSame($sum, $result, "multiply function not multiplying correctly");
    }

    public function test_multiply_argument_one_cannot_take_in_array_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::multiply(["abc"], 10);
    }

    public function test_multiply_argument_one_cannot_take_in_string_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::multiply("abc", 10);
    }

    public function test_multiply_argument_one_cannot_take_in_null_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::multiply(null, 10);
    }

    public function test_multiply_argument_one_cannot_take_in_boolean_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::multiply(true, 10);
    }

    public function test_multiply_argument_one_cannot_take_in_function_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::multiply(fn() => true, 10);
    }

    public function test_multiply_argument_two_cannot_take_in_array_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::multiply(10, ["abc"]);
    }

    public function test_multiply_argument_two_cannot_take_in_string_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::multiply(10, "abc");
    }

    public function test_multiply_argument_two_cannot_take_in_null_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::multiply(10, null);
    }

    public function test_multiply_argument_two_cannot_take_in_boolean_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::multiply(10, true);
    }

    public function test_multiply_argument_two_cannot_take_in_function_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::multiply(10, fn() => true);
    }

    public function test_divide_can_divide_numbers()
    {
        $num1 = 10;
        $num2 = 5;
        $sum = $num1 / $num2;

        $result = ArithmeticHelper::divide($num1, $num2);

        $this->assertSame($sum, $result, "divide function not dividing correctly");
    }

    public function test_divide_argument_one_cannot_take_in_array_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::divide(["abc"], 10);
    }

    public function test_divide_argument_one_cannot_take_in_string_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::divide("abc", 10);
    }

    public function test_divide_argument_one_cannot_take_in_null_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::divide(null, 10);
    }

    public function test_divide_argument_one_cannot_take_in_boolean_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::divide(true, 10);
    }

    public function test_divide_argument_one_cannot_take_in_function_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::divide(fn() => true, 10);
    }

    public function test_divide_argument_two_cannot_take_in_array_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::divide(10, ["abc"]);
    }

    public function test_divide_argument_two_cannot_take_in_string_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::divide(10, "abc");
    }

    public function test_divide_argument_two_cannot_take_in_null_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::divide(10, null);
    }

    public function test_divide_argument_two_cannot_take_in_boolean_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::divide(10, true);
    }

    public function test_divide_argument_two_cannot_take_in_function_argument()
    {
        $this->expectException(\InvalidArgumentException::class);
        $result = ArithmeticHelper::divide(10, fn() => true);
    }
}
