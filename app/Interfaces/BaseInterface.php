<?php

namespace App\Interfaces;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;

interface BaseInterface
{
    public function all(bool $includeSoftDeleted = false, array $with = [], bool $paginate = false, int $paginationCount = 15, int $page = 1);

    public function find(int $id);

    public function query() : Builder;

    public function create($request, Closure $callback = null);

    public function update($request, int $id = null, Closure $callback = null) : Model;

    public function delete(int $id, Closure $callback = null) : bool;

    public function first() : Model;

    public function returnResource(string $className, $data) : JsonResource;
}
