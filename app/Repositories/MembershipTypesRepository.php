<?php

namespace App\Repositories;

use App\Interfaces\MembershipTypeInterface;
use App\Models\MembershipType;

class MembershipTypesRepository extends BaseRepository implements MembershipTypeInterface
{
    public function __construct(MembershipType $membershipType)
    {
        parent::__construct($membershipType);
    }

}
