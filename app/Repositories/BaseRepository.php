<?php

namespace App\Repositories;

use App\Interfaces\BaseInterface;
use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class BaseRepository implements BaseInterface
{
    /**
     * @var Model
     */
    private Model $model;

    /**
     * @var int
     */
    private int $defaultPaginationCount;

    /**
     * BaseRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->defaultPaginationCount = config('app.pagination_default_count');
    }

    public function all(bool $includeSoftDeleted = false, array $with = [], bool $paginate = false, int $paginationCount = null, int $page = 1, Closure $callback = null)
    {
        $paginationCount = empty($paginationCount) ? $this->defaultPaginationCount : $paginationCount;

        $query = $this->model->with($with);

        if ($includeSoftDeleted) {
            $this->model = $query->withTrashed();
        }

        if ($callback) {
            $callback($query);
        }

        if ($paginate) {
            return $query->paginate($paginationCount, ['*'], 'page', $page);
        } else {
            return $query->get();
        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function find(int $id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @return Builder
     */
    public function query() : Builder
    {
        return $this->model->query();
    }

    /**
     * @param $request
     * @param Closure|null $callback
     * @return mixed
     */
    public function create($request, Closure $callback = null)
    {
        return DB::transaction(function () use ($request, $callback) {
            $this->model = $this->model->create($request);

            if ($callback) {
                $callback($request, $this->model);
            }

            return $this->model;
        });
    }

    /**
     * @param $request
     * @param int|null $id
     * @param Closure|null $callback
     * @return Model
     */
    public function update($request, int $id = null, Closure $callback = null) : Model
    {
        return DB::transaction(function () use ($request, $id, $callback) {
            $this->model = $this->find($id);

            $this->model->update($request);

            if ($callback) {
                $callback($request, $this->model);
            }

            return $this->model->refresh();
        });
    }

    /**
     * @param int $id
     * @param Closure|null $callback
     * @return bool
     */
    public function delete(int $id, Closure $callback = null) : bool
    {
        return DB::transaction(function () use ($id, $callback) {
            $this->model = $this->find($id);

            if ($callback) {
                $callback($id, $this->model);
            }

            return $this->model->delete();
        });
    }

    /**
     * @return Model
     */
    public function first() : Model
    {
        return $this->model->first();
    }

    public function returnResource(string $className, $data) : JsonResource
    {
        if ($data instanceof Model) {
            return new $className($data);
        } else {
            return $className::collection($data);
        }
    }
}
