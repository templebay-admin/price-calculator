<?php

namespace App\Repositories;

use App\Interfaces\VenueInterface;
use App\Models\Venue;

class VenueRepository extends BaseRepository implements VenueInterface
{
    public function __construct(Venue $venue)
    {
        parent::__construct($venue);
    }

}
