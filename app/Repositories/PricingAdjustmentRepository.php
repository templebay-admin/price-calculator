<?php

namespace App\Repositories;

use App\Helpers\Math\ModifyHelper;
use App\Interfaces\PricingAdjustmentInterface;
use App\Models\PricingAdjustment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PricingAdjustmentRepository extends BaseRepository implements PricingAdjustmentInterface
{
    public function __construct(PricingAdjustment $pricingAdjustment)
    {
        parent::__construct($pricingAdjustment);
    }

    public function create($request, $callback = null)
    {
        return DB::transaction(function () use ($request, $callback) {

            return PricingAdjustment::create([
                'venue_id' => $request->venue_id,
                'pricing_adjustment_type' => $request->pricing_adjustment_type,
                'low_age' => $request->low_age,
                'high_age' => $request->high_age,
                'modify_amount' => ModifyHelper::convert($request->modify_amount),
                'pricing_option_id' => $request->pricing_option_id,
                'membership_type_id' => $request->membership_type_id
            ]);

        });
    }

    public function update($request, int $id = null, $callback = null) : Model
    {

        return DB::transaction(function () use ($request, $id, $callback) {
            $pricingAdjustment = $this->find($id);

            $pricingAdjustment->update([
                'venue_id' => $request->venue_id,
                'pricing_adjustment_type' => $request->pricing_adjustment_type,
                'low_age' => $request->low_age,
                'high_age' => $request->high_age,
                'modify_amount' => ModifyHelper::convert($request->modify_amount),
                'pricing_option_id' => $request->pricing_option_id,
                'membership_type_id' => $request->membership_type_id
            ]);

            return $pricingAdjustment->refresh();
        });
    }

}
