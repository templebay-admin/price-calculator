<?php

namespace App\Repositories;

use App\Helpers\Math\ArithmeticHelper;
use App\Helpers\Math\ModifyHelper;
use App\Interfaces\PurchasableInterface;
use App\Models\Purchasable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PurchasableRepository extends BaseRepository implements PurchasableInterface
{
    public function __construct(Purchasable $purchasable)
    {
        parent::__construct($purchasable);
    }

    public function create($request, $callback = null)
    {
        return DB::transaction(function () use ($request, $callback) {

            $purchasable = Purchasable::create([
                'name' => $request->name,
                'type' => $request->purchasable_type_id,
            ]);

            foreach($request->pricingOptions as $pricingOption){
                $purchasable->pricingOptions()->attach($pricingOption,[
                    'basic_price' => ArithmeticHelper::multiply(ModifyHelper::convert($request->basic_price[$pricingOption->id]), 100)
                ]);
            }

            return $purchasable;
        });
    }

    public function update($request, int $id = null, $callback = null) : Model
    {

        return DB::transaction(function () use ($request, $id, $callback) {
            $purchasable = $this->find($id);

            if($request->purchasable_type_id != $purchasable->id){
                foreach($purchasable->pricingOptions as $pricingOption){
                    $purchasable->pricingOptions()->detach($pricingOption);
                }

                foreach($purchasable->pricingOptions as $pricingOption){
                    $purchasable->pricingOptions()->attach($pricingOption,[
                        'basic_price' => ArithmeticHelper::multiply(ModifyHelper::convert($request->basic_price[$pricingOption->id]), 100)
                    ]);
                }

            } else {
                foreach($request->pricingOptions as $pricingOption){
                    $purchasable->pricingOptions()->updateExistingPivot($pricingOption, [
                        'basic_price' => ArithmeticHelper::multiply(ModifyHelper::convert($request->basic_price[$pricingOption->id]), 100)
                    ]);
                }
            }

            $purchasable->update($request->all());

            return $purchasable->refresh();
        });
    }

}
