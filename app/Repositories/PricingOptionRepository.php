<?php

namespace App\Repositories;

use App\Interfaces\PricingOptionInterface;
use App\Models\PricingOption;

class PricingOptionRepository extends BaseRepository implements PricingOptionInterface
{
    public function __construct(PricingOption $pricingOption)
    {
        parent::__construct($pricingOption);
    }

}
