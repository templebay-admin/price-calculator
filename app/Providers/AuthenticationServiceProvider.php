<?php

namespace App\Providers;

use App\Actions\CreateNewUser;
use App\Authentication;
use App\Http\Responses\LockoutResponse;
use App\Http\Responses\LoginResponse;
use App\Http\Responses\LogoutResponse;
use App\Http\Responses\RegisterResponse;
use App\Models\MembershipType;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\ServiceProvider;

use App\Contracts\LoginResponse as LoginResponseContract;
use App\Contracts\LogoutResponse as LogoutResponseContract;
use App\Contracts\LockoutResponse as LockoutResponseContract;
use App\Contracts\RegisterResponse as RegisterResponseContract;

class AuthenticationServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../../config/base-authentication.php', 'authentication');

        $this->registerResponseBindings();

        $this->app->bind(StatefulGuard::class, function () {
            return Auth::guard(config('authentication.guard', null));
        });
    }

    /**
     * Register the response bindings.
     *
     * @return void
     */
    protected function registerResponseBindings()
    {
        $this->app->singleton(LockoutResponseContract::class, LockoutResponse::class);
        $this->app->singleton(LoginResponseContract::class, LoginResponse::class);
        $this->app->singleton(LogoutResponseContract::class, LogoutResponse::class);
        $this->app->singleton(RegisterResponseContract::class, RegisterResponse::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Authentication::registerView(function () {
            $membershipTypes = MembershipType::all();
            return view('auth.register', compact('membershipTypes'));
        });
        Authentication::loginView(function () {
            return view('auth.login');
        });

        Authentication::createUsersUsing(CreateNewUser::class);

        RateLimiter::for('login', function (Request $request) {
            $email = (string) $request->email;

            return Limit::perMinute(5)->by($email.$request->ip());
        });

        RateLimiter::for('two-factor', function (Request $request) {
            return Limit::perMinute(5)->by($request->session()->get('login.id'));
        });
    }
}
