<?php

namespace App\Providers;

use App\Interfaces\BaseInterface;
use App\Interfaces\MembershipTypeInterface;
use App\Interfaces\PricingAdjustmentInterface;
use App\Interfaces\PricingOptionInterface;
use App\Interfaces\PurchasableInterface;
use App\Interfaces\VenueInterface;
use App\Repositories\BaseRepository;
use App\Repositories\MembershipTypesRepository;
use App\Repositories\PricingAdjustmentRepository;
use App\Repositories\PricingOptionRepository;
use App\Repositories\PurchasableRepository;
use App\Repositories\VenueRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BaseInterface::class, BaseRepository::class);
        $this->app->bind(VenueInterface::class, VenueRepository::class);
        $this->app->bind(MembershipTypeInterface::class, MembershipTypesRepository::class);
        $this->app->bind(PricingAdjustmentInterface::class, PricingAdjustmentRepository::class);
        $this->app->bind(PurchasableInterface::class, PurchasableRepository::class);
        $this->app->bind(PricingOptionInterface::class, PricingOptionRepository::class);
    }
}
