<?php

namespace App\Contracts;

use Illuminate\Contracts\Support\Responsable;

interface RegisterResponse extends Responsable
{
    //
}
