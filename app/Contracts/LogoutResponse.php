<?php

namespace App\Contracts;

use Illuminate\Contracts\Support\Responsable;

interface LogoutResponse extends Responsable
{
    //
}
