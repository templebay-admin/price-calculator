<?php

namespace App\Contracts;

use Illuminate\Contracts\Support\Responsable;

interface LockoutResponse extends Responsable
{
    //
}
