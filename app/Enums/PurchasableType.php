<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static FitnessClass()
 * @method static static Product()
 * @method static static Consumable()
 */
final class PurchasableType extends Enum
{
    const FitnessClass =   1;
    const Product =   2;
    const Consumable = 3;
}
