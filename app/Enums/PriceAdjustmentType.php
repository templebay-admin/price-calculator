<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Multiply()
 * @method static static Reduce()
 * @method static static Fixed()
 */
final class PriceAdjustmentType extends Enum
{
    const Multiply =   1;
    const Reduce =   2;
    const Fixed = 3;
}
