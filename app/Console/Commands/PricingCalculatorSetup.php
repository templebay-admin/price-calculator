<?php

namespace App\Console\Commands;

use App\Console\Commands\Traits\PrettyCommandOutput;
use Illuminate\Console\Command;
use Illuminate\Encryption\Encrypter;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Process\Process;

class PricingCalculatorSetup extends Command
{

    use PrettyCommandOutput;

    protected $progressBar;


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pricing-setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command tests, migrates and seeds the pricing calculator app ready for browser testing';

    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    protected Filesystem $files;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->box('Welcome to pricing calculator');

        $this->info(' Please wait whilst we get your project ready');

        //Copy .env
        exec('cp .env.example .env');
        $this->info(' .env copied');

        //Clear cache
        exec('php artisan cache:clear');
        exec('php artisan config:clear');

        //Generate key
        $key = $this->generateRandomKey();
        if (! $this->setKeyInEnvironmentFile($key)) {
            $this->error('There has been an issue, hang fire while we try to fix.');
        }

        $this->laravel['config']['app.key'] = $key;

        $this->info('Key successfully generated');
        $this->info($key);


        $this->progressBar = $this->output->createProgressBar(4);
        $this->progressBar->minSecondsBetweenRedraws(0);
        $this->progressBar->maxSecondsBetweenRedraws(120);
        $this->progressBar->setRedrawFrequency(1);

        $this->progressBar->start();

        $this->info(' Pricing calculator setup has started. Please wait...');
        $this->progressBar->advance();

        exec('php artisan test');
        $this->info(' Testing complete');

        $this->info(' Migrating databases');
        exec('php artisan migrate:fresh');
        $this->info(' Migration complete');

        $this->info(' Seeding database');
        exec('php artisan db:seed');
        $this->info(' Seeding complete');

        $this->progressBar->finish();

        $this->info(' Pricing calculator setup has finished.');
    }

    /**
     * Generate a random key for the application.
     *
     * @return string
     */
    protected function generateRandomKey()
    {
        return 'base64:'.base64_encode(
                Encrypter::generateKey($this->laravel['config']['app.cipher'])
            );
    }

    /**
     * Set the application key in the environment file.
     *
     * @param  string  $key
     * @return bool
     */
    protected function setKeyInEnvironmentFile($key)
    {
        $this->writeNewEnvironmentFileWith($key);

        return true;
    }

    /**
     * Write a new environment file with the given key.
     *
     * @param  string  $key
     * @return void
     */
    protected function writeNewEnvironmentFileWith($key)
    {
        file_put_contents($this->laravel->environmentFilePath(), preg_replace(
            $this->keyReplacementPattern(),
            'APP_KEY='.$key,
            file_get_contents($this->laravel->environmentFilePath())
        ));

        file_put_contents(base_path().DIRECTORY_SEPARATOR.'.env.testing', preg_replace(
            $this->keyReplacementPattern(),
            'APP_KEY='.$key,
            file_get_contents(base_path().DIRECTORY_SEPARATOR.'.env.testing')
        ));
    }

    /**
     * Get a regex pattern that will match env APP_KEY with any random key.
     *
     * @return string
     */
    protected function keyReplacementPattern()
    {
        $escaped = preg_quote('='.$this->laravel['config']['app.key'], '/');

        return "/^APP_KEY{$escaped}/m";
    }
}
