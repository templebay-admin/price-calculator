<?php

namespace App\Actions;

use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use App\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param array $input
     * @return User
     * @throws ValidationException
     */
    public function create(array $input): User
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
            'membership_type_id' => [
                'required',
                'numeric',
                'exists:membership_types,id'
            ],
            'date_of_birth' => [
                'required',
                'date'
            ],
            'password' => 'required|confirmed',
        ])->validate();

        return User::create([
            'name'               => $input['name'],
            'email'              => $input['email'],
            'membership_type_id' => $input['membership_type_id'],
            'date_of_birth'      => $input['date_of_birth'],
            'password'           => $input['password'],
        ]);
    }
}
