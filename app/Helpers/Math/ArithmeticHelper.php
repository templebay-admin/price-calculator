<?php

namespace App\Helpers\Math;

class ArithmeticHelper
{
    /**
     * Sum multiple numbers together
     *
     * @param ...$nums
     * @return float|int
     */
    public static function add(...$nums): float|int
    {
        if(sizeof($nums) < 1){
            throw new \InvalidArgumentException("Must have at least one argument");
        }

        $sum = 0;

        foreach($nums as $num){
            if(!(is_float($num) || is_int($num))){
                throw new \InvalidArgumentException("Argument can only be numeric");
            }

            $sum += $num;
        }

        return $sum;
    }

    /**
     * Minus a number from another number
     *
     * @param $num1
     * @param $num2
     * @return float|int
     */
    public static function minus($num1, $num2): float|int
    {
        if(!(is_float($num1) || is_int($num1))){
            throw new \InvalidArgumentException("Argument one can only be numeric");
        }

        if(!(is_float($num2) || is_int($num2))){
            throw new \InvalidArgumentException("Argument two can only be numeric");
        }

        return $num1 - $num2;
    }

    /**
     * Multiply two numbers
     *
     * @param $num1
     * @param $num2
     * @return float|int
     */
    public static function multiply($num1, $num2): float|int
    {
        if(!(is_float($num1) || is_int($num1))){
            throw new \InvalidArgumentException("Argument can only be numeric");
        }

        if(!(is_float($num2) || is_int($num2))){
            throw new \InvalidArgumentException("Argument two can only be numeric");
        }

        return $num1 * $num2;
    }

    /**
     * Divide two numbers
     *
     * @param $num1
     * @param $num2
     * @return float|int
     */
    public static function divide($num1, $num2): float|int
    {
        if(!(is_float($num1) || is_int($num1))){
            throw new \InvalidArgumentException("Argument can only be numeric");
        }

        if(!(is_float($num2) || is_int($num2))){
            throw new \InvalidArgumentException("Argument two can only be numeric");
        }

        return $num1 / $num2;
    }
}
