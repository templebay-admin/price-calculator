<?php

namespace App\Helpers\Math;

class ModifyHelper
{

    /**
     * Convert string to float|int
     *
     * @param $input
     * @return float|int
     */
    public static function convert($input): float|int
    {
        if ((int) $input == $input) {
            return intval($input);
        } else {
            return floatval($input);
        }
    }

    /**
     * @param $number
     * @return string
     */
    public static function formatNumberToString($number): string
    {
        return number_format($number, 2);
    }
}
