<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Venue extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    /**
     * Return related pricing adjustments
     *
     * @return HasMany
     */
    public function pricingAdjustments(): HasMany
    {
        return $this->hasMany(PricingAdjustment::class);
    }

    /**
     * return related purchasables
     *
     * @return BelongsToMany
     */
    public function purchasables(): BelongsToMany
    {
        return $this->belongsToMany(Purchasable::class, 'pricing_option_purchasable_venue')->withPivot('basic_price', 'pricing_option_id');
    }
}
