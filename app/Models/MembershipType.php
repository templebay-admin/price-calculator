<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class MembershipType extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    /**
     * Return the related users
     *
     * @return HasMany
     */
    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }

    /**
     * Return the related membership types
     *
     * @return BelongsToMany
     */
    public function priceAdjustments(): BelongsToMany
    {
        return $this->belongsToMany(PricingAdjustment::class, 'membership_type_pricing_adjustment');
    }
}
