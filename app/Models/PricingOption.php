<?php

namespace App\Models;

use App\Enums\PurchasableType;
use App\Helpers\Math\ArithmeticHelper;
use App\Helpers\Math\ModifyHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class PricingOption extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    /**
     * return related purchasables
     *
     * @return BelongsToMany
     */
    public function purchasables(): BelongsToMany
    {
        return $this->belongsToMany(Purchasable::class)->withPivot('basic_price');
    }

    /**
     *
     */
    public function basePrice(): string
    {
        return $this->formattedDisplayPrice($this->pivot->basic_price);
    }

    /**
     * @param $venue
     * @return string
     */
    public function price($venue): string
    {
        return $this->getAdjustedPrice($venue);
    }

    /**
     * @param $venue
     * @return string
     */
    public function getAdjustedPrice($venue)
    {
        $user = auth()->user();

        $priceAdjustment = PricingAdjustment::where('venue_id', $venue->id)
            ->where('pricing_option_id', $this->id)
            ->first();

        if($priceAdjustment){
            if($priceAdjustment->userIsWithinAgeBracket($user)){
                if($priceAdjustment->membershipType->id == $user->membership_type_id){
                    return $priceAdjustment->formattedDisplayPriceForOption($this->pivot->basic_price);
                }
            }
        }

        return $this->formattedDisplayPrice($this->pivot->basic_price);
    }

    /**
     * @param $price
     * @return string
     */
    public function formattedDisplayPrice($price): string
    {
        return '£'. ModifyHelper::formatNumberToString(ArithmeticHelper::divide($price,100));
    }



    public static function getOptions($purchasableTypeId)
    {
        if($purchasableTypeId == PurchasableType::Consumable || $purchasableTypeId == PurchasableType::Product){
            return PricingOption::where('name', '!=', 'Standard')->get();
        } else {
            return PricingOption::where('name', 'Standard')->get();
        }
    }
}
