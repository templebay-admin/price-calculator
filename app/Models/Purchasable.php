<?php

namespace App\Models;

use App\Enums\PurchasableType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Purchasable extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'type',
    ];

    /**
     * Return related pricing options
     *
     * @return BelongsToMany
     */
    public function pricingOptions(): BelongsToMany
    {
        return $this->belongsToMany(PricingOption::class, 'pricing_option_purchasable')->withPivot('basic_price');
    }


}
