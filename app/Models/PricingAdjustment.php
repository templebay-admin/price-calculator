<?php

namespace App\Models;

use App\Enums\PriceAdjustmentType;
use App\Helpers\Math\ArithmeticHelper;
use App\Helpers\Math\ModifyHelper;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Hash;

class PricingAdjustment extends Model
{
    use HasFactory;

    //The modify amount field is the figure in which the base price is to be modified by.
    //To allow for maximum accuracy, if the price adjustment type is of Reduce or Fixed then the modify_amount
    //is stored as the currencies lowest form (pence).
    //We use a mutator to mutate the amount before storing (£1.50 will be stored as 150)
    //If the adjustment type is that of Multiply then we need to adjust the modify_amount before we work with it, back into its original state.
    // Multiple amount is set at 0.75, stored into the db as 75, so when we work with it we need to mutate it back to 0.75.


    protected $fillable = [
        'venue_id',
        'pricing_adjustment_type',
        'low_age',
        'high_age',
        'modify_amount',
        'pricing_option_id',
        'membership_type_id',
    ];

    /**
     * modify modify_amount.
     *
     * @return Attribute
     */
    protected function modifyAmount(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => ArithmeticHelper::multiply($value,100),
        );
    }

    /**
     * Return the related membership types
     *
     * @return BelongsTo
     */
    public function membershipType(): BelongsTo
    {
        return $this->belongsTo(MembershipType::class);
    }

    /**
     * return related venue
     *
     * @return BelongsTo
     */
    public function venue(): BelongsTo
    {
        return $this->belongsTo(Venue::class);
    }

    /**
     * return related pricing option
     *
     * @return BelongsTo
     */
    public function pricingOption(): BelongsTo
    {
        return $this->belongsTo(PricingOption::class);
    }

    /**
     * @param $user
     * @return bool
     */
    public function userIsWithinAgeBracket($user)
    {
        $age = $user->age();

        if($age >= $this->low_age && $age <= $this->high_age){
            return true;
        }

        return false;
    }

    /**
     * @param $basicPrice
     * @return string
     */
    public function formattedDisplayPriceForOption($basicPrice)
    {
        return $this->formattedDisplayPrice($this->adjustedPrice($basicPrice));
    }

    /**
     * @param $price
     * @return string
     */
    public function formattedDisplayPrice($price): string
    {
        return '£'. ModifyHelper::formatNumberToString($price);
    }

    /**
     * @param $basicPrice
     * @return float|int|void
     */
    public function adjustedPrice($basicPrice)
    {
        switch($this->pricing_adjustment_type){
            case PriceAdjustmentType::Multiply:
                return ArithmeticHelper::divide(ArithmeticHelper::multiply(
                    $basicPrice,
                    ArithmeticHelper::divide($this->modify_amount,100)),
                    100);
            case PriceAdjustmentType::Reduce:
                return ArithmeticHelper::divide(ArithmeticHelper::minus(
                    $basicPrice,
                    $this->modify_amount),
                    100);
            case PriceAdjustmentType::Fixed:
                ArithmeticHelper::divide($this->modify_amount, 100);
        }
    }

    /**
     * @return string
     */
    public function ageBracket()
    {
        return $this->low_age . ' - ' . $this->high_age;
    }

    /**
     * @return string|void
     */
    public function adjustedBy()
    {
        switch($this->pricing_adjustment_type){
            case PriceAdjustmentType::Multiply:
                return 'x' . ArithmeticHelper::divide($this->modify_amount,100);
            case PriceAdjustmentType::Reduce:
                return '-' . ArithmeticHelper::divide($this->modify_amount, 100);
            case PriceAdjustmentType::Fixed:
                return 'Fixed price ' . ArithmeticHelper::divide($this->modify_amount, 100);
        }
    }



}
