<?php

namespace App\Http\Requests;

use App\Authentication;
use Illuminate\Foundation\Http\FormRequest;

class PurchasableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->is_admin;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'purchasable_type_id' => 'required'
        ];
    }
}
