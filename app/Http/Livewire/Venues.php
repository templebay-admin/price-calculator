<?php

namespace App\Http\Livewire;

use App\Models\Venue;
use App\Repositories\VenueRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Livewire\Component;

class Venues extends Component
{
    /**
     * @var $name
     */
    public $name;

    /**
     * @var Collection $venues
     */
    public Collection $venues;

    /**
     * @var bool $notInEditMode
     */
    public bool $notInEditMode = true;

    /**
     * @var $editingVenue
     */
    public $editingVenue = null;

    /**
     * @var $editingVenueName
     */
    public $editingVenueName = null;

    /**
     * Validation rules
     * @var string[]
     */
    protected array $rules = [
        'name' => 'required|max:255',
    ];

    /**
     * Mount the component
     */
    public function mount()
    {
        $this->reloadVenues();
    }

    /**
     * Handle creating the venue
     */
    public function submit()
    {
        $this->validate();

        $venueRepository = App::make(VenueRepository::class);

        $venueRepository->create(['name' => $this->name]);

        $this->name = null;

        $this->reloadVenues();
    }

    /**
     * Handle updating the venue
     */
    public function update()
    {
        $venueRepository = App::make(VenueRepository::class);

        $venueRepository->update(['name' => $this->editingVenueName], $this->editingVenue->id);

        $this->resetEditFields();

        $this->reloadVenues();
    }

    /**
     * Reset the edit attributes
     */
    public function resetEditFields()
    {
        $this->notInEditMode = true;
        $this->editingVenue = null;
        $this->editingVenueName = null;
    }

    /**
     * cancel editing the venue
     */
    public function cancelEditing()
    {
        $this->resetEditFields();
    }

    /**
     * Edit the venue
     *
     * @param $venueId
     */
    public function edit($venueId)
    {
        $this->notInEditMode = false;
        $this->editingVenue = Venue::find($venueId);
        $this->editingVenueName = $this->editingVenue->name;

    }

    /**
     * Reload and set the venues
     */
    public function reloadVenues()
    {
        $this->venues = Venue::all();
    }

    /**
     * @return Application|Factory|View
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.venues');
    }
}
