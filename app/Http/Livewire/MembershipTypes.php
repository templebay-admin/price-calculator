<?php

namespace App\Http\Livewire;

use App\Models\MembershipType;
use App\Repositories\MembershipTypesRepository;
use App\Repositories\VenueRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Livewire\Component;

class MembershipTypes extends Component
{
    /**
     * @var $name
     */
    public $name;

    /**
     * @var Collection $membershipTypes
     */
    public Collection $membershipTypes;

    /**
     * @var bool $notInEditMode
     */
    public bool $notInEditMode = true;

    /**
     * @var $editingMembershipType
     */
    public $editingMembershipType = null;

    /**
     * @var $editingMembershipTypeName
     */
    public $editingMembershipTypeName = null;

    /**
     * Validation rules
     * @var array
     */
    protected array $rules = [
        'name' => 'required|max:255',
    ];

    /**
     * mount the component
     *
     */
    public function mount()
    {
        $this->reloadMembershipTypes();
    }

    /**
     * Handle submission of create form
     */
    public function submit()
    {
        $this->validate();

        $membershipTypeRepository = App::make(MembershipTypesRepository::class);

        $membershipTypeRepository->create(['name' => $this->name]);

        $this->name = null;

        $this->reloadMembershipTypes();

    }

    /**
     * Handle submission of update form
     */
    public function update()
    {

        $membershipTypeRepository = App::make(MembershipTypesRepository::class);

        $membershipTypeRepository->update(['name' => $this->editingMembershipTypeName], $this->editingMembershipType->id);

        $this->resetEditFields();

        $this->reloadMembershipTypes();
    }

    /**
     * Reset edit attributes
     */
    public function resetEditFields()
    {
        $this->notInEditMode = true;
        $this->editingMembershipType = null;
        $this->editingMembershipTypeName = null;
    }

    /**
     * Cacnel editing membership type
     */
    public function cancelEditing()
    {
        $this->resetEditFields();
    }

    /**
     * Edit membership type
     *
     * @param $membershipTypeId
     */
    public function edit($membershipTypeId)
    {
        $this->notInEditMode = false;
        $this->editingMembershipType = MembershipType::find($membershipTypeId);
        $this->editingMembershipTypeName = $this->editingMembershipType->name;

    }

    /**
     * Reload membership types
     */
    public function reloadMembershipTypes()
    {
        $this->membershipTypes = MembershipType::all();
    }

    /**
     * @return Application|Factory|View
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.membership-types');
    }
}
