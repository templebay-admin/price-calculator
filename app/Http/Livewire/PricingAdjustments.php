<?php

namespace App\Http\Livewire;

use App\Enums\PriceAdjustmentType;
use App\Helpers\Math\ArithmeticHelper;
use App\Models\MembershipType;
use App\Models\PricingAdjustment;
use App\Models\PricingOption;
use App\Models\Venue;
use App\Repositories\PricingAdjustmentRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Livewire\Component;

class PricingAdjustments extends Component
{
    /**
     * @var Collection $pricingAdjustments
     */
    public Collection $pricingAdjustments;

    /**
     * @var Collection $pricingOptions
     */
    public Collection $pricingOptions;

    /**
     * @var Collection $venues
     */
    public Collection $venues;

    /**
     * @var Collection $membershipTypes
     */
    public Collection $membershipTypes;

    /**
     * @var $venue_id
     */
    public $venue_id = '';

    /**
     * @var $membership_type_id
     */
    public $membership_type_id = '';

    /**
     * @var $pricing_option_id
     */
    public $pricing_option_id = '';

    /**
     * @var $low_age
     */
    public $low_age = null;

    /**
     * @var $high_age
     */
    public $high_age = null;

    /**
     * @var $pricing_adjustment_type
     */
    public $pricing_adjustment_type = '';

    /**
     * @var $modify_amount
     */
    public $modify_amount = null;

    /**
     * @var $adjustmentTypes
     */
    public $adjustmentTypes = [];

    /**
     * @var bool $notInEditMode
     */
    public bool $notInEditMode = true;

    /**
     * @var $editingPricingAdjustment
     */
    public $editingPricingAdjustment = null;

    /**
     * @var $editing_venue_id
     */
    public $editing_venue_id = '';

    /**
     * @var $editing_membership_type_id
     */
    public $editing_membership_type_id = '';

    /**
     * @var $editing_pricing_option_id
     */
    public $editing_pricing_option_id = '';

    /**
     * @var $editing_low_age
     */
    public $editing_low_age = '';

    /**
     * @var $editing_high_age
     */
    public $editing_high_age = '';

    /**
     * @var $editing_pricing_adjustment_type
     */
    public $editing_pricing_adjustment_type = '';

    /**
     * @var $editing_modify_amount
     */
    public $editing_modify_amount = '';


    /**
     * @var array
     */
    protected array $rules = [
        'venue_id'            => 'required',
        'membership_type_id' => 'required',
        'pricing_option_id' => 'required',
        'low_age' => 'required',
        'high_age' => 'required',
        'pricing_adjustment_type' => 'required',
        'modify_amount' => 'required',
    ];

    /**
     * Mount the component
     */
    public function mount()
    {
        $this->pricingAdjustments = PricingAdjustment::all();
        $this->venues = Venue::all();
        $this->pricingOptions = PricingOption::all();
        $this->membershipTypes = MembershipType::all();
        $this->reloadPricingAdjustments();

        $this->adjustmentTypes = [
            [
                'id' => PriceAdjustmentType::Multiply,
                'name' => 'Multiply'
            ],
            [
                'id' => PriceAdjustmentType::Reduce,
                'name' => 'Reduce'
            ],
            [
                'id' => PriceAdjustmentType::Fixed,
                'name' => 'Fixed amount'
            ]
        ];
    }

    /**
     * Edit pricing adjustment
     * @param $pricingAdjustmentId
     */
    public function edit($pricingAdjustmentId)
    {
        $this->notInEditMode = false;
        $this->editingPricingAdjustment = PricingAdjustment::find($pricingAdjustmentId);
        $this->editing_venue_id = $this->editingPricingAdjustment->venue->id;
        $this->editing_membership_type_id = $this->editingPricingAdjustment->membershipType->id;
        $this->editing_pricing_option_id = $this->editingPricingAdjustment->pricingOption->id;
        $this->editing_low_age = $this->editingPricingAdjustment->low_age;
        $this->editing_high_age = $this->editingPricingAdjustment->high_age;
        $this->editing_pricing_adjustment_type = $this->editingPricingAdjustment->pricing_adjustment_type;
        $this->editing_modify_amount = ArithmeticHelper::divide($this->editingPricingAdjustment->modify_amount,100);
    }

    /**
     * Handles submission of create form
     */
    public function submit()
    {
        $this->validate();

        $request = new Request();

        $request->venue_id = $this->venue_id;
        $request->membership_type_id = $this->membership_type_id;
        $request->pricing_option_id = $this->pricing_option_id;
        $request->low_age = $this->low_age;
        $request->high_age = $this->high_age;
        $request->pricing_adjustment_type = $this->pricing_adjustment_type;
        $request->modify_amount = $this->modify_amount;

        $pricingAdjustmentRepository = App::make(PricingAdjustmentRepository::class);

        $pricingAdjustmentRepository->create($request);

        $this->venue_id = '';
        $this->membership_type_id = '';
        $this->pricing_option_id = '';
        $this->low_age = null;
        $this->high_age = null;
        $this->pricing_adjustment_type = '';
        $this->modify_amount =  null;

        $this->reloadPricingAdjustments();

    }

    /**
     * Handles submission of update form
     */
    public function update()
    {
        $request = new Request();

        $request->venue_id = $this->editing_venue_id;
        $request->membership_type_id = $this->editing_membership_type_id;
        $request->pricing_option_id = $this->editing_pricing_option_id;
        $request->low_age = $this->editing_low_age;
        $request->high_age = $this->editing_high_age;
        $request->pricing_adjustment_type = $this->editing_pricing_adjustment_type;
        $request->modify_amount = $this->editing_modify_amount;

        $pricingAdjustmentRepository = App::make(PricingAdjustmentRepository::class);

        $pricingAdjustmentRepository->update($request, $this->editingPricingAdjustment->id);

        $this->resetEditFields();

        $this->reloadPricingAdjustments();

    }


    /**
     * reset the edit attributes
     *
     */
    public function resetEditFields()
    {
        $this->notInEditMode = true;
        $this->editingPricingAdjustment = null;
        $this->editing_venue_id = '';
        $this->editing_membership_type_id = '';
        $this->editing_pricing_option_id = '';
        $this->editing_low_age = null;
        $this->editing_high_age = null;
        $this->editing_pricing_adjustment_type = '';
        $this->editing_modify_amount = null;
    }

    /**
     * Cancel editing pricing adjustment
     */
    public function cancelEditing()
    {
        $this->resetEditFields();
    }

    /**
     * Reload pricing adjustments
     */
    public function reloadPricingAdjustments()
    {
        $this->pricingAdjustments = PricingAdjustment::all();
    }

    /**
     * @return Application|Factory|View
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.pricing-adjustments');
    }
}
