<?php

namespace App\Http\Livewire;

use App\Enums\PurchasableType;
use App\Models\PricingOption;
use App\Models\Purchasable;
use App\Repositories\PurchasableRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Livewire\Component;

class Purchasables extends Component
{
    /**
     * @var $name
     */
    public $name;

    /**
     * @var Collection $purchasables
     */
    public Collection $purchasables;

    /**
     * @var array $purchasableTypes
     */
    public array $purchasableTypes;

    /**
     * @var $purchasable_type_id
     */
    public $purchasable_type_id = '';

    /**
     * @var bool $showPricingOptions
     */
    public bool $showPricingOptions = false;

    /**
     * @var Collection $pricingOptions
     */
    public $pricingOptions = null;

    /**
     * @var $basic_price
     */
    public $basic_price = [];

    /**
     * @var bool $notInEditMode
     */
    public bool $notInEditMode = true;

    /**
     * @var $editingPurchasable
     */
    public $editingPurchasable = null;

    /**
     * @var $editingPurchasableName
     */
    public $editingPurchasableName = null;

    /**
     * @var $editing_purchasable_type
     */
    public $editing_purchasable_type = '';

    /**
     * @var $editing_basic_price
     */
    public $editing_basic_price = [];

    /**
     * @var $editingPricingOptions
     */
    public $editingPricingOptions = null;


    /**
     * Validation rules
     * @var array
     */
    protected array $rules = [
        'name' => 'required|max:255',
        'purchasable_type_id' => 'required',
    ];

    /**
     * mount the component
     *
     */
    public function mount()
    {

        $this->purchasableTypes = [
            [
                'id' => PurchasableType::Product,
                'name' => 'Product'
            ],
            [
                'id' => PurchasableType::FitnessClass,
                'name' => 'Fitness Class'
            ],
            [
                'id' => PurchasableType::Consumable,
                'name' => 'Consumable'
            ]
        ];

        $this->reloadPurchasables();
    }

    /**
     * $purchasable_type_id updated, retrieve pricing options based on purchasable type
     */
    public function updatedPurchasableTypeId()
    {
        $this->pricingOptions = PricingOption::getOptions($this->purchasable_type_id);
        $this->showPricingOptions = true;
    }

    /**
     * Handle submission of the create form
     */
    public function submit()
    {
        $this->validate();

        $request = new Request();

        $request->name = $this->name;
        $request->purchasable_type_id = $this->purchasable_type_id;
        $request->pricingOptions = $this->pricingOptions;
        $request->basic_price = $this->basic_price;

        $purchasableRepository = App::make(PurchasableRepository::class);

        $purchasableRepository->create($request);

        $this->name = null;
        $this->purchasable_type_id = '';
        $this->showPricingOptions = false;
        $this->basic_price = [];

        $this->reloadPurchasables();
    }

    /**
     * Handle submission of the update form
     */
    public function update()
    {
        $request = new Request();

        $request->name = $this->editingPurchasableName;
        $request->purchasable_type_id = $this->editing_purchasable_type;
        $request->pricingOptions = $this->editingPricingOptions;
        $request->basic_price = $this->editing_basic_price;

        $purchasableRepository = App::make(PurchasableRepository::class);

        $purchasableRepository->update($request, $this->editingPurchasable->id);

        $this->resetEditFields();

        $this->reloadPurchasables();
    }

    /**
     * Reset the edit attrinutes
     */
    public function resetEditFields()
    {
        $this->notInEditMode = true;
    }

    /**
     * cacnel editing purchasable
     */
    public function cancelEditing()
    {
        $this->resetEditFields();
    }

    /**
     * Edit a purchasable
     */
    public function edit($purchasableId)
    {
        $this->notInEditMode = false;
        $this->editingPurchasable = Purchasable::find($purchasableId);
        $this->editingPurchasableName = $this->editingPurchasable->name;
        $this->editing_purchasable_type = $this->editingPurchasable->type;
        $this->editingPricingOptions = $this->editingPurchasable->pricingOptions;

        foreach($this->editingPricingOptions as $pricingOption){
            $this->editing_basic_price[$pricingOption->id] = $pricingOption->pivot->basic_price/100;
        }

    }

    /**
     * Reload the purchasables
     */
    public function reloadPurchasables()
    {
        $this->purchasables = Purchasable::all();
    }

    /**
     * @return Application|Factory|View
     */
    public function render(): View|Factory|Application
    {
        return view('livewire.purchasables');
    }
}
