<?php

namespace App\Http\Controllers;

use App\Http\Requests\PurchasableRequest;
use App\Models\Purchasable;
use App\Repositories\PurchasableRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PurchasableController extends Controller
{

    /**
     * Purchasable Repository
     *
     * @var PurchasableRepository;
     */
    public PurchasableRepository $purchasableRepository;

    /**
     * Purchasable Controller Constructor
     *
     * @param PurchasableRepository $purchasableRepository
     */
    public function __construct(PurchasableRepository $purchasableRepository)
    {
        $this->purchasableRepository = $purchasableRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('admin.purchasables.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PurchasableRequest $request
     * @return void
     */
    public function store(PurchasableRequest $request)
    {
        $this->purchasableRepository->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PurchasableRequest $request
     * @param Purchasable $purchasable
     * @return void
     */
    public function update(PurchasableRequest $request, Purchasable $purchasable)
    {
        $this->purchasableRepository->update($request, $purchasable->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Purchasable $purchasable
     * @return void
     */
    public function destroy(Purchasable $purchasable)
    {
        $this->purchasableRepository->delete($purchasable->id);
    }
}
