<?php

namespace App\Http\Controllers;

use App\Http\Requests\VenueRequest;
use App\Models\Venue;
use App\Repositories\VenueRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class VenueController extends Controller
{
    /**
     * Venue Repository
     *
     * @var VenueRepository;
     */
    public VenueRepository $venueRepository;

    /**
     * Venue Controller Constructor
     *
     * @param VenueRepository $venueRepository
     */
    public function __construct(VenueRepository $venueRepository)
    {
        $this->venueRepository = $venueRepository;
    }

    /**
     * Display a listing of the resource.
     * (resource is being retrieved in Livewire component)
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        return view('admin.venues.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param VenueRequest $request
     * @return void
     */
    public function store(VenueRequest $request)
    {
        $this->venueRepository->create($request->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param VenueRequest $request
     * @param Venue $venue
     * @return void
     */
    public function update(VenueRequest $request, Venue $venue)
    {
        $this->venueRepository->update($request->all(), $venue->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Venue $venue
     * @return void
     */
    public function destroy(Venue $venue)
    {
        $this->venueRepository->delete($venue->id);
    }
}
