<?php

namespace App\Http\Controllers;

use App\Models\PricingAdjustment;
use App\Repositories\PricingAdjustmentRepository;
use App\Repositories\PurchasableRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PricingAdjustmentController extends Controller
{

    /**
     * Pricing Adjustment Repository
     *
     * @var PricingAdjustmentRepository;
     */
    public PricingAdjustmentRepository $pricingAdjustmentRepository;

    /**
     * Pricing Adjustment Controller Constructor
     *
     * @param PricingAdjustmentRepository $pricingAdjustmentRepository
     */
    public function __construct(PricingAdjustmentRepository $pricingAdjustmentRepository)
    {
        $this->pricingAdjustmentRepository = $pricingAdjustmentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        return view('admin.pricing-adjustments.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function store(Request $request)
    {
        $this->pricingAdjustmentRepository->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param PricingAdjustment $priceAdjustment
     * @return void
     */
    public function update(Request $request, PricingAdjustment $priceAdjustment)
    {
        $this->pricingAdjustmentRepository->update($request, $priceAdjustment->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PricingAdjustment $priceAdjustment
     * @return void
     */
    public function destroy(PricingAdjustment $priceAdjustment)
    {
        $this->pricingAdjustmentRepository->delete($priceAdjustment->id);
    }
}
