<?php

namespace App\Http\Controllers;

use App\Http\Requests\MembershipTypeRequest;
use App\Models\MembershipType;
use App\Repositories\MembershipTypesRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class MembershipTypeController extends Controller
{
    /**
     * MembershipTypes Repository
     *
     * @var MembershipTypesRepository;
     */
    public MembershipTypesRepository $membershipTypesRepository;

    /**
     * MembershipTypes Controller Constructor
     *
     * @param MembershipTypesRepository $membershipTypesRepository
     */
    public function __construct(MembershipTypesRepository $membershipTypesRepository)
    {
        $this->membershipTypesRepository = $membershipTypesRepository;
    }

    /**
     * Display a listing of the resource.
     * (resource is being retrieved in Livewire component)
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        return view('admin.memberships.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MembershipTypeRequest $request
     * @return void
     */
    public function store(MembershipTypeRequest $request)
    {
        $this->membershipTypesRepository->create($request->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  MembershipTypeRequest $request
     * @param MembershipType $membershipType
     * @return void
     */
    public function update(MembershipTypeRequest $request, MembershipType $membershipType)
    {
        $this->membershipTypesRepository->update($request->all(), $membershipType->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param MembershipType $membershipType
     * @return void
     */
    public function destroy(MembershipType $membershipType)
    {
        $this->membershipTypesRepository->delete($membershipType->id);
    }
}
