<?php

namespace App\Http\Controllers;

use App\Http\Requests\PricingOptionRequest;
use App\Models\PricingOption;
use App\Repositories\PricingOptionRepository;
use Illuminate\Http\Response;

class PricingOptionController extends Controller
{

    /**
     * Pricing Option Repository
     *
     * @var PricingOptionRepository;
     */
    public PricingOptionRepository $pricingOptionRepository;

    /**
     * Pricing Option Controller Constructor
     *
     * @param PricingOptionRepository $pricingOptionRepository
     */
    public function __construct(PricingOptionRepository $pricingOptionRepository)
    {
        $this->pricingOptionRepository = $pricingOptionRepository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PricingOptionRequest $request
     * @return void
     */
    public function store(PricingOptionRequest $request)
    {
        $this->pricingOptionRepository->create($request->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PricingOptionRequest  $request
     * @param PricingOption $pricingOption
     * @return void
     */
    public function update(PricingOptionRequest $request, PricingOption $pricingOption)
    {
        $this->pricingOptionRepository->update($request->all(), $pricingOption->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PricingOption $pricingOption
     * @return void
     */
    public function destroy(PricingOption $pricingOption)
    {
        $this->pricingOptionRepository->delete($pricingOption->id);
    }
}
