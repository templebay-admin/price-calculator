<?php

namespace App\Http\Controllers\Auth;

use App\Authentication;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Routing\Pipeline;
use App\Actions\AttemptToAuthenticate;
use App\Actions\EnsureLoginIsNotThrottled;
use App\Actions\PrepareAuthenticatedSession;
use App\Contracts\LoginResponse;
use App\Contracts\LoginViewResponse;
use App\Contracts\LogoutResponse;
use App\Http\Requests\LoginRequest;

class AuthenticatedSessionController extends Controller
{
    /**
     * The guard implementation.
     *
     * @var StatefulGuard
     */
    protected StatefulGuard $guard;

    /**
     * Create a new controller instance.
     *
     * @param StatefulGuard $guard
     * @return void
     */
    public function __construct(StatefulGuard $guard)
    {
        $this->guard = $guard;
    }

    /**
     * Show the login view.
     *
     * @return LoginViewResponse
     */
    public function create(): LoginViewResponse
    {
        return app(LoginViewResponse::class);
    }

    /**
     * Attempt to authenticate a new session.
     *
     * @param LoginRequest $request
     * @return mixed
     */
    public function store(LoginRequest $request): mixed
    {
        return $this->loginPipeline($request)->then(function () {
            return app(LoginResponse::class);
        });
    }

    /**
     * Get the authentication pipeline instance.
     *
     * @param LoginRequest $request
     * @return \Illuminate\Pipeline\Pipeline
     */
    protected function loginPipeline(LoginRequest $request): \Illuminate\Pipeline\Pipeline
    {
        if (Authentication::$authenticateThroughCallback) {
            return (new Pipeline(app()))->send($request)->through(array_filter(
                call_user_func(Authentication::$authenticateThroughCallback, $request)
            ));
        }

        if (is_array(config('authentication.pipelines.login'))) {
            return (new Pipeline(app()))->send($request)->through(array_filter(
                config('authentication.pipelines.login')
            ));
        }

        return (new Pipeline(app()))->send($request)->through(array_filter([
            config('authentication.limiters.login') ? null : EnsureLoginIsNotThrottled::class,
            null,
            AttemptToAuthenticate::class,
            PrepareAuthenticatedSession::class,
        ]));
    }

    /**
     * Destroy an authenticated session.
     *
     * @param Request $request
     * @return LogoutResponse
     */
    public function destroy(Request $request): LogoutResponse
    {
        $this->guard->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return app(LogoutResponse::class);
    }
}
