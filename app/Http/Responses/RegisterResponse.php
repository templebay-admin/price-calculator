<?php

namespace App\Http\Responses;

use App\Authentication;
use Illuminate\Http\JsonResponse;
use App\Contracts\RegisterResponse as RegisterResponseContract;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RegisterResponse implements RegisterResponseContract
{
    /**
     * Create an HTTP response that represents the object.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function toResponse($request): JsonResponse|Response
    {
        return $request->wantsJson()
            ? new JsonResponse('', 201)
            : redirect()->intended(Authentication::redirects('register'));
    }
}
