<?php

namespace App\Http\Responses;

use App\Authentication;
use Illuminate\Http\JsonResponse;
use App\Contracts\LogoutResponse as LogoutResponseContract;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LogoutResponse implements LogoutResponseContract
{
    /**
     * Create an HTTP response that represents the object.
     *
     * @param  Request  $request
     * @return Response
     */
    public function toResponse($request)
    {
        return $request->wantsJson()
            ? new JsonResponse('', 204)
            : redirect(Authentication::redirects('logout', '/'));
    }
}
