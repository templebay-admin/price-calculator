<?php

namespace App;


use App\Contracts\CreatesNewUsers;
use App\Contracts\LoginViewResponse;
use App\Contracts\RegisterViewResponse;;
use App\Http\Responses\SimpleViewResponse;

class Authentication
{
    /**
     * The callback that is responsible for building the authentication pipeline array, if applicable.
     *
     * @var callable|null
     */
    public static $authenticateThroughCallback;

    /**
     * The callback that is responsible for validating authentication credentials, if applicable.
     *
     * @var callable|null
     */
    public static $authenticateUsingCallback;

    /**
     * The callback that is responsible for confirming user passwords.
     *
     * @var callable|null
     */
    public static $confirmPasswordsUsingCallback;

    /**
     * Get the username used for authentication.
     *
     * @return string
     */
    public static function username(): string
    {
        return config('authentication.username', 'email');
    }

    /**
     * Get the name of the email address request variable / field.
     *
     * @return string
     */
    public static function email(): string
    {
        return config('authentication.email', 'email');
    }

    /**
     * Specify which view should be used as the login view.
     *
     * @param  callable|string  $view
     * @return void
     */
    public static function loginView($view)
    {
        app()->singleton(LoginViewResponse::class, function () use ($view) {
            return new SimpleViewResponse($view);
        });
    }

    /**
     * Specify which view should be used as the registration view.
     *
     * @param callable|string $view
     * @return void
     */
    public static function registerView(callable|string $view)
    {
        app()->singleton(RegisterViewResponse::class, function () use ($view) {
            return new SimpleViewResponse($view);
        });
    }

    /**
     * Register a class / callback that should be used to create new users.
     *
     * @param  string  $callback
     * @return void
     */
    public static function createUsersUsing(string $callback)
    {
        app()->singleton(CreatesNewUsers::class, $callback);
    }

    /**
     * Get a completion redirect path for a specific feature.
     *
     * @param string $redirect
     * @param null $default
     * @return string
     */
    public static function redirects(string $redirect, $default = null): string
    {
        return config('authentication.redirects.'.$redirect) ?? $default ?? config('authentication.home');
    }


}
