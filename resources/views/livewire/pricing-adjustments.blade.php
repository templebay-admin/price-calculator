<div>
    <div id="main" class="main-content flex-1 mt-12 md:mt-2 pb-24 md:pb-5">
        <div class="bg-gray-800 pt-3">
            <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
                <h1 class="font-bold pl-2">Purchasables</h1>
            </div>
        </div>


        <div class="flex flex-wrap">
            <div class="w-1/2 lg:w-1/2 md:w-1/2 xl:w-1/3 p-6">
                <table class="min-w-full">
                    <thead class="bg-gray-50 dark:bg-gray-700">
                    <tr>
                        <th scope="col" class="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                            Venue
                        </th>
                        <th scope="col" class="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                            Adjustment Type
                        </th>
                        <th scope="col" class="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                            Adjusted by
                        </th>
                        <th scope="col" class="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                            Age bracket
                        </th>
                        <th scope="col" class="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                            pricing option
                        </th>

                        <th scope="col" class="relative py-3 px-6">
                            <span class="sr-only">Edit</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pricingAdjustments as $adjustment)
                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                            <td class="py-4 px-6 text-sm font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $adjustment->venue->name }}
                            </td>
                            <td class="py-4 px-6 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                                {{ \App\Enums\PriceAdjustmentType::getKey($adjustment->pricing_adjustment_type) }}
                            </td>
                            <td class="py-4 px-6 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                                {{ $adjustment->adjustedBy() }}
                            </td>
                            <td class="py-4 px-6 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                                {{ $adjustment->ageBracket()}}
                            </td>
                            <td class="py-4 px-6 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                                {{ $adjustment->pricingOption->name }} options
                            </td>
                            <td class="py-4 px-6 text-sm font-medium text-right whitespace-nowrap">
                                <button wire:click="edit({{ $adjustment->id }})" class="text-blue-600 dark:text-blue-500 hover:underline">Edit</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            @if($notInEditMode)
                <div class="w-1/2 lg:w-1/2 md:w-1/2 xl:w-1/3 p-6">
                <h2 class="text-white">Create a new Pricing adjustment</h2>
                <form class="rounded pt-6 pb-8 mb-4" wire:submit.prevent="submit">
                    <div class="mb-4">
                        <label class="block text-white text-sm font-bold mb-2" for="venue_id">
                            Venue
                        </label>
                        <select class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="venue_id" wire:model="venue_id" id="venue_id">
                            <option value="" selected disabled>Select a venue</option>
                            @foreach($venues as $venue)
                                <option value="{{ $venue->id }}">{{ $venue->name }}</option>
                            @endforeach
                        </select>
                        @error('venue_id') <p class="text-white">{{ $message }}</p> @enderror
                    </div>

                    <div class="mb-4">
                        <label class="block text-white text-sm font-bold mb-2" for="membership_type_id">
                            Membership type
                        </label>
                        <select class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="membership_type_id" wire:model="membership_type_id" id="membership_type_id">
                            <option value="" selected disabled>Select a membership</option>
                            @foreach($membershipTypes as $membershipType)
                                <option value="{{ $membershipType->id }}">{{ $membershipType->name }}</option>
                            @endforeach
                        </select>
                        @error('membership_type_id') <p class="text-white">{{ $message }}</p> @enderror
                    </div>

                    <div class="mb-4">
                        <label class="block text-white text-sm font-bold mb-2" for="pricing_option_id">
                            Pricing Option
                        </label>
                        <select class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="pricing_option_id" wire:model="pricing_option_id" id="pricing_option_id">
                            <option value="" selected disabled>Select a membership</option>
                            @foreach($pricingOptions as $pricingOption)
                                <option value="{{ $pricingOption->id }}">{{ $pricingOption->name }}</option>
                            @endforeach
                        </select>
                        @error('pricing_option_id') <p class="text-white">{{ $message }}</p> @enderror
                    </div>

                    <div class="mb-4">
                        <label class="block text-white text-sm font-bold mb-2" for="low_age">
                            Age From
                        </label>
                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="low_age" wire:model="low_age" type="number" step="1" max="99" placeholder="Age from">
                        @error('low_age') <p class="text-white">{{ $message }}</p> @enderror
                    </div>

                    <div class="mb-4">
                        <label class="block text-white text-sm font-bold mb-2" for="high_age">
                            Age To
                        </label>
                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="high_age" wire:model="high_age" type="number" step="1" max="99" placeholder="Age To">
                        @error('high_age') <p class="text-white">{{ $message }}</p> @enderror
                    </div>

                    <div class="mb-4">
                        <label class="block text-white text-sm font-bold mb-2" for="price_adjustment_type">
                            Price adjustment type
                        </label>
                        <select class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="pricing_adjustment_type" wire:model="pricing_adjustment_type" id="pricing_adjustment_type">
                            <option value="" selected disabled>Select a price adjustment type</option>
                            @foreach($adjustmentTypes as $adjustmentType)
                                <option value="{{ $adjustmentType['id'] }}">{{ $adjustmentType['name'] }}</option>
                            @endforeach
                        </select>
                        @error('pricing_adjustment_type') <p class="text-white">{{ $message }}</p> @enderror
                    </div>

                    <div class="mb-4">
                        <label class="block text-white text-sm font-bold mb-2" for="modify_amount">
                            Adjustment amount
                        </label>
                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="modify_amount" wire:model="modify_amount" type="number" step="0.01" placeholder="Adjustment amount">
                        @error('modify_amount') <p class="text-white">{{ $message }}</p> @enderror
                    </div>

                    <div class="flex items-center justify-between">
                        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                            Add Pricing adjustment
                        </button>
                    </div>
                </form>

            </div>
            @else
                <div class="w-1/2 lg:w-1/2 md:w-1/2 xl:w-1/3 p-6">
                    <h2 class="text-white">Editing Pricing adjustment</h2>
                    <form class="rounded pt-6 pb-8 mb-4" wire:submit.prevent="update">
                        <div class="mb-4">
                            <label class="block text-white text-sm font-bold mb-2" for="editing_venue_id">
                                Venue
                            </label>
                            <select class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="editing_venue_id" wire:model="editing_venue_id" id="editing_venue_id">
                                <option value="" selected disabled>Select a venue</option>
                                @foreach($venues as $venue)
                                    <option value="{{ $venue->id }}">{{ $venue->name }}</option>
                                @endforeach
                            </select>
                            @error('venue_id') <p class="text-white">{{ $message }}</p> @enderror
                        </div>

                        <div class="mb-4">
                            <label class="block text-white text-sm font-bold mb-2" for="editing_membership_type_id">
                                Membership type
                            </label>
                            <select class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="editing_membership_type_id" wire:model="editing_membership_type_id" id="editing_membership_type_id">
                                <option value="" selected disabled>Select a membership</option>
                                @foreach($membershipTypes as $membershipType)
                                    <option value="{{ $membershipType->id }}">{{ $membershipType->name }}</option>
                                @endforeach
                            </select>
                            @error('membership_type_id') <p class="text-white">{{ $message }}</p> @enderror
                        </div>

                        <div class="mb-4">
                            <label class="block text-white text-sm font-bold mb-2" for="editing_pricing_option_id">
                                Pricing Option
                            </label>
                            <select class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="editing_pricing_option_id" wire:model="editing_pricing_option_id" id="editing_pricing_option_id">
                                <option value="" selected disabled>Select a membership</option>
                                @foreach($pricingOptions as $pricingOption)
                                    <option value="{{ $pricingOption->id }}">{{ $pricingOption->name }}</option>
                                @endforeach
                            </select>
                            @error('pricing_option_id') <p class="text-white">{{ $message }}</p> @enderror
                        </div>

                        <div class="mb-4">
                            <label class="block text-white text-sm font-bold mb-2" for="editing_low_age">
                                Age From
                            </label>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="editing_low_age" wire:model="editing_low_age" type="number" step="1" max="99" placeholder="Age from">
                            @error('low_age') <p class="text-white">{{ $message }}</p> @enderror
                        </div>

                        <div class="mb-4">
                            <label class="block text-white text-sm font-bold mb-2" for="editing_high_age">
                                Age To
                            </label>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="editing_high_age" wire:model="editing_high_age" type="number" step="1" max="99" placeholder="Age To">
                            @error('high_age') <p class="text-white">{{ $message }}</p> @enderror
                        </div>

                        <div class="mb-4">
                            <label class="block text-white text-sm font-bold mb-2" for="editing_price_adjustment_type">
                                Price adjustment type
                            </label>
                            <select class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="editing_pricing_adjustment_type" wire:model="editing_pricing_adjustment_type" id="editing_pricing_adjustment_type">
                                <option value="" selected disabled>Select a price adjustment type</option>
                                @foreach($adjustmentTypes as $adjustmentType)
                                    <option value="{{ $adjustmentType['id'] }}">{{ $adjustmentType['name'] }}</option>
                                @endforeach
                            </select>
                            @error('pricing_adjustment_type') <p class="text-white">{{ $message }}</p> @enderror
                        </div>

                        <div class="mb-4">
                            <label class="block text-white text-sm font-bold mb-2" for="editing_modify_amount">
                                Adjustment amount
                            </label>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="editing_modify_amount" wire:model="editing_modify_amount" type="number" step="0.01" placeholder="Adjustment amount">
                            @error('modify_amount') <p class="text-white">{{ $message }}</p> @enderror
                        </div>

                        <div class="flex items-center justify-between">
                            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                                Add Pricing adjustment
                            </button>
                        </div>
                    </form>
                    <div class="flex items-center justify-between">
                        <button type="button" wire:click="cancelEditing" class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                            Cancel editing
                        </button>
                    </div>

                </div>
            @endif
        </div>
    </div>
</div>

