<div>
    <div id="main" class="main-content flex-1 mt-12 md:mt-2 pb-24 md:pb-5">
        <div class="bg-gray-800 pt-3">
            <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
                <h1 class="font-bold pl-2">Purchasables</h1>
            </div>
        </div>


        <div class="flex flex-wrap">
            <div class="w-1/2 lg:w-1/2 md:w-1/2 xl:w-1/3 p-6">
                <table class="shadow-md rounded px-8 pt-6 pb-8 mb-4">
                    <thead class="bg-gray-50 dark:bg-gray-700">
                    <tr>
                        <th scope="col" class="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                            Name
                        </th>
                        <th scope="col" class="py-3 px-6 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                            Type
                        </th>
                        <th scope="col" class="relative py-3 px-6">
                            <span class="sr-only">Edit</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($purchasables as $purchasable)
                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                            <td class="py-4 px-6 text-sm font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $purchasable->name }}
                            </td>
                            <td class="py-4 px-6 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                                {{ \App\Enums\PurchasableType::getKey($purchasable->type) }}
                            </td>
                            <td class="py-4 px-6 text-sm font-medium text-right whitespace-nowrap">
                                <button wire:click="edit({{ $purchasable->id }})" class="text-blue-600 dark:text-blue-500 hover:underline">Edit</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            @if($notInEditMode)
                <div class="w-1/2 lg:w-1/2 md:w-1/2 xl:w-1/3 p-6">
                <h2 class="text-white">Create a new purchasable</h2>
                <form class="rounded pt-6 pb-8 mb-4" wire:submit.prevent="submit">
                    <div class="mb-4">
                        <label class="block text-white text-sm font-bold mb-2" for="name">
                            Purchasable name
                        </label>
                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="name" wire:model="name" type="text" placeholder="Purchasable name">
                        @error('name') <p class="text-white">{{ $message }}</p> @enderror
                    </div>
                    <div class="mb-4">
                        <label class="block text-white text-sm font-bold mb-2" for="purchasable_type">
                            Purchasable type
                        </label>
                        <select class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="purchasable_type" wire:model="purchasable_type_id" id="purchasable_type">
                            <option value="" selected disabled>Select a purchasable type</option>
                            @foreach($purchasableTypes as $purchasableType)
                                <option value="{{ $purchasableType['id'] }}">{{ $purchasableType['name'] }}</option>
                            @endforeach
                        </select>
                        @error('purchasable_type_id') <p class="text-white">{{ $message }}</p> @enderror
                    </div>

                    @if($showPricingOptions)
                        <div class="mb-4">
                            @foreach($pricingOptions as $option)
                                <label class="block text-white text-sm font-bold mb-2">
                                    {{ $option->name }} base price
                                </label>
                                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" wire:model="basic_price.{{ $option->id }}" type="number" step=".01" placeholder="base price">
                            @endforeach
                        </div>
                    @endif
                    <div class="flex items-center justify-between">
                        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                            Add purchasable
                        </button>
                    </div>
                </form>

            </div>
            @else
                <div class="w-1/2 lg:w-1/2 md:w-1/2 xl:w-1/3 p-6">
                    <h2 class="text-white">Editing purchasable</h2>
                    <form class="rounded pt-6 pb-8 mb-4" wire:submit.prevent="update">
                        <div class="mb-4">
                            <label class="block text-white text-sm font-bold mb-2" for="name">
                                Purchasable name
                            </label>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="name" wire:model="editingPurchasableName" type="text" placeholder="Purchasable name">
                            @error('editingPurchasableName') <p class="text-white">{{ $message }}</p> @enderror
                        </div>
                        <div class="mb-4">
                            <label class="block text-white text-sm font-bold mb-2" for="editing_purchasable_type">
                                Purchasable type
                            </label>
                            <select class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="editing_purchasable_type" wire:model="editing_purchasable_type" id="editing_purchasable_type">
                                <option value="" selected disabled>Select a purchasable type</option>
                                @foreach($purchasableTypes as $purchasableType)
                                    <option value="{{ $purchasableType['id'] }}">{{ $purchasableType['name'] }}</option>
                                @endforeach
                            </select>
                            @error('editing_purchasable_type') <p class="text-white">{{ $message }}</p> @enderror
                        </div>


                        <div class="mb-4">
                            @foreach($editingPricingOptions as $option)
                                <label class="block text-white text-sm font-bold mb-2">
                                    {{ $option->name }} base price
                                </label>
                                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" wire:model="editing_basic_price.{{ $option->id }}" type="number" step=".01" placeholder="base price">
                            @endforeach
                        </div>

                        <div class="flex items-center justify-between">
                            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                                Update purchasable
                            </button>
                        </div>
                    </form>
                    <div class="flex items-center justify-between">
                        <button type="button" wire:click="cancelEditing" class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                            Cancel editing
                        </button>
                    </div>

                </div>
            @endif
        </div>
    </div>
</div>

