<?php


return [
    'guard' => 'web',
    'middleware' => ['web'],
    'auth_middleware' => 'auth',
    'passwords' => 'users',
    'username' => 'email',
    'email' => 'email',
    'views' => true,
    'home' => '/home',
    'prefix' => '',
    'domain' => null,
    'limiters' => [
        'login' => null,
    ],
    'redirects' => [
        'login' => '/admin/dashboard',
        'logout' => null,
        'password-confirmation' => null,
        'register' => '/admin/dashboard',
        'email-verification' => null,
        'password-reset' => null,
    ],
];
